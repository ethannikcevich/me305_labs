"""
Created on Thu Jan  6 12:32:37 2022
Author: Ethan Nikcevich
@file: Nikcevich_lab1
"""

def fib(idx):
    if int(idx)==0:
        fibonacci = 0
    elif int(idx)==1:
        fibonacci = 1
    #Check if the index is 0 or 1, which are both explicitly defined.
    else:
        i0 = 0
        i1 = 1
        # Fibonacci sequence can be expressed as adding the current number to 
        # the previous one in the sequence, starting with 0 and 1. 
        n = 1
        # n is a counter that will increase by 1 after each iteration.
        while n < int(idx):
        # Once n equals the index, the loop will end.
            fibonacci = i0 + i1
            # Each iteration finds the next number in the sequence...
            i0 = i1
            i1 = fibonacci
            # and shifts i0 and i1 along the sequence.
            n += 1
            # The count is increased by 1
    return fibonacci
    # The number in the sequence corresponding with the index is returned

if __name__ == '__main__':
    x = 1
    # Create var x to check if it is the user's first entry
    while True:
    # while True to create a loop that runs indefinitely
        if x == 1:
            idx = str(input('Enter an index: '))
            # Prompt the user to give an index
            if str(idx).isnumeric():
            # Check if the user's index entry is a positive integer
                print ('Fibonacci number at '
                       'index {:} is {:}.'.format(idx,fib(idx)))
                # Call fib() and print the result for the user
            else:
                print('The chosen index is not a positive integer!')
        else:
            idx = input('Enter a new index to continue, or enter q to exit the program: ')
            # Prompt the user to enter a new index, which only occurs when the count (x) is no longer 1
            if idx == 'q': 
                break
                # End the program when the user enters q
            elif str(idx).isnumeric():
                print ('Fibonacci number at '
                           'index {:} is {:}.'.format(idx,fib(idx)))
            else:
                print('The chosen index is not a positive integer!')
        x += 1