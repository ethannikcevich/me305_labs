'''!@file                   task_touchpad.py
    @brief                  File for all touch panel interactions
    @details                Responsible for interfacing with the resistive touch panel using an object from touchpad.py
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   3/17/22
'''
   
from time import ticks_us, ticks_add, ticks_diff
import micropython
import os
import gc
from ulab import numpy


S0_INIT = micropython.const(0)
S1_RUN = micropython.const (1)
S2_CALIBRATE = micropython.const (2)
S3_SET_CALIBRATION_VALUES = micropython.const (3)
S4_END_CALIBRATION = micropython.const (4)

center = micropython.const(0)
top_left = micropython.const(1)
top_right = micropython.const(2)
bot_left = micropython.const(3)
bot_right = micropython.const(4)

def run(taskname, period, coord, Pad, filter_data, Alpha, Beta, calibration_check):
    '''!@brief                    Calibrates the touchpad and finds the ball's position and velocity
        @details                  Calibrates and then repeatedly calculates the position and velocity of the ball on the touchpad throuh the use of alpha beta filtering to correct for shear, rotation, and displacement errors.
        @param taskname           To run multiple tasks
        @param period             period of how frequently the task is run
        @param coord              x, y, and z coordinate values
        @param Pad                Touchpad object
        @param filter_data        For filtering using alpha and  beta.
        @param Alpha              Alpha is used in filtering the touchpad data
        @param Beta               Beta is used in filtering the touchpad data
        @param calibration_check  Checks if the system needs to be calibrated
    '''
    
    state = S0_INIT
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    # For calibrating the touchpad and reading from a calibrated txt file
    calibration_txt = "Touchpad_cal_coeffs.txt"
    calibration_str = ''
    
    index = 0
    location = center
    ticks = 0
    x_k = 0
    y_k = 0
    z_c = 0
    z_f = 0
    v_xk = 0
    v_yk = 0
     
    while True:
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                
                if calibration_txt in os.listdir():
                    state = S4_END_CALIBRATION
                else:
                    if calibration_check.read():
                        print('Calibrate the touchpad by pressing the specified locations.')
                        print('Press the center of the panel.')
                        state = S2_CALIBRATE
                                
                
            elif state == S1_RUN:
                Ts = ticks_diff(current_time, ticks)*(1E-6)
                ticks = ticks_us()
                filter_data.write((-x_k, -v_xk, y_k, v_yk, z_f))
                coord.write(Pad.Read())
                x, y, z = coord.read()
                if z == 0:
                    x_k += 0
                    y_k += 0
                    z_c += 1
                    z_f += 0
                    v_xk += 0
                    v_yk += 0
                    if z_c > 10:
                        x_k = 0
                        y_k = 0
                        z_f = 0
                        v_xk = 0
                        v_yk = 0
                elif z == 1:
                    if z_c != 0:
                        if z_c >= 10:
                            x_k = x
                            y_k = y
                            z_f = 1
                        z_c = 0
                    x_k1 = x_k + Alpha*(x - x_k) + Ts*v_xk
                    v_xk1 = v_xk + (Beta/Ts)*(x - x_k)
                    y_k1 = y_k + Alpha*(y-y_k) + Ts*v_yk
                    v_yk1 = v_yk + (Beta/Ts)*(y - y_k)
                    x_k = x_k1
                    y_k = y_k1
                    v_xk = v_xk1
                    v_yk = v_yk1
                    
            elif state == S2_CALIBRATE:
                if location == center:
                    x_Center, y_Center, z_Center = Pad.Read()
                    if z_Center == 1:
                        x_d, y_d, z_F = Pad.Read()
                        if z_F == 0:
                            location = top_left
                            print('Press the top left corner')
                elif location == top_left:
                    x_top_left, y_top_left, z_top_left = Pad.Read()
                    if z_top_left == 1:
                        x_d, y_d, z_F = Pad.Read()
                        if z_F == 0:
                            location = top_right
                            print('Press the top right corner')
                elif location == top_right:
                    x_top_right, y_top_right, z_top_right = Pad.Read()
                    if z_top_right == 1:
                        x_d,y_d,z_F = Pad.Read()
                        if z_F == 0:    
                            location = bot_left
                            print('Press the bottom left corner')
                elif location == bot_left:
                    x_bot_left, y_bot_left, z_bot_left = Pad.Read()
                    if z_bot_left == 1:
                        x_d, y_d, z_F = Pad.Read()
                        if z_F == 0:
                            location = bot_right
                            print('Press the bottom right corner')
                elif location == bot_right:
                    x_bot_right, y_bot_right, z_bot_right = Pad.Read()
                    if z_bot_right == 1:
                        x_d, y_d, z_F = Pad.Read()
                        if z_F == 0:
                            location = center
                            state = S3_SET_CALIBRATION_VALUES
                    
            elif state == S3_SET_CALIBRATION_VALUES:
                X = numpy.array([[x_Center, y_Center, 1], [x_top_left, y_top_left, 1], [x_top_right, y_top_right, 1], [x_bot_left, y_bot_left, 1], [x_bot_right, y_bot_right, 1]])
                print(X)
                X_T = X.transpose()
                Y = numpy.array([[0, 0], [-80, 40], [80, 40], [-80, -40], [80, -40]])
                print(Y)
                X_TX = numpy.dot(X_T, X)
                X_TX1 = numpy.linalg.inv(X_TX)
                X_TX1_X_T = numpy.dot(X_TX1, X_T)
                beta = numpy.dot(X_TX1_X_T, Y)
                print(beta)
                K_xx = beta[0][0]
                K_yy = beta[1][1]
                K_xy = beta[1][0]
                K_yx = beta[0][1]
                x_o = beta[2][0]
                y_o = beta[2][1]
                Pad.calibrate(K_xx, K_xy, x_o, K_yx, K_yy, y_o)
                
                with open(calibration_txt, 'w') as f:
                    buf = [K_xx, K_xy, x_o, K_yx, K_yy, y_o]
                    gc.collect()
                    for element in buf:
                        calibration_str += str(element)
                        calibration_str += ','
                        index += 1
                    if index == 6:
                        print(calibration_str)
                        print('written to file')
                        calibration_str = calibration_str[:-1]
                        f.write(calibration_str)
                        state = S1_RUN
                        ticks = ticks_us
                        
            elif state == S4_END_CALIBRATION:
                with open(calibration_txt, 'r') as f:
                    cal_data_string = f.readline()
                    print(cal_data_string)
                    cal_values = [float(cal_value) for cal_value in cal_data_string.strip().split(',')]
                    print(cal_values)
                    K_xx = cal_values[0]
                    K_xy = cal_values[1]
                    x_o = cal_values[2]
                    K_yx = cal_values[3]
                    K_yy = cal_values[4]
                    y_o = cal_values[5]
                    
                    Pad.calibrate(K_xx, K_xy, x_o, K_yx, K_yy, y_o)
                state = S1_RUN
                ticks = ticks_us
            next_time = ticks_add(next_time, period)
            yield state
        else:
            yield None