'''!@file       BNO055.py
    @brief      Enables the IMU and its functions
    @details    Establishes a BNO055 Class for interaction with the IMU
    @author     Ethan Nikcevich
    @author     Johnathan Dietz
    @date       3/17/22
'''
from pyb import I2C

class BNO055:
    '''!@brief      Driver class for BNO055
        @details    Creates objects of the BNO055 IMU to retrieve sensor data.
    '''

    def __init__ (self):
        '''!@brief    Creates a BNO055 object
            @details  Establishes I2C and needed attributes for calibration and retrieving sensor data
        '''
        ## @brief   Create I2C object
        #  @details Create I2C object and init as a controller
        self.i2c = I2C(1, I2C.CONTROLLER)
        ## @brief   fusion mode with 9 DOF
        #  @details Absolute orientation data is calculated from accelerometer, gyroscope, and the magnetometer.
        self.ndof = 12
        ## @brief   A byte to be set to the first byte of the i2c calibration
        #  @details Used to separate the mag, acc, gyr, and sys values.
        self.cal_byte = 0
        # Set to configuration mode
        self.i2c.mem_write(0, 0x28, 0x3D)
        self.i2c.mem_write(0x21, 0x28, 0x41)
        self.i2c.mem_write(0x02, 0x28, 0x42)
        
        ## @brief   6 byte array for euler data
        #  @details three 2-byte euler values are passed into the array
        self.euler_buffer = bytearray(6*[0])
        ## @brief   6 byte array for gyro data
        #  @details three 2-byte gyro values are passed into the array
        self.velocity_buffer = bytearray(6*[0])
        

    def change_mode(self, mode):
        '''!@brief         Set the IMU to a passed fusion mode.
            @details       A method to change the operating mode of the IMU to one of the many “fusion” modes
                           available from the BNO055.
            @param mode    The fusion mode that the IMU is to be set to
        '''        
        self.i2c.mem_write(mode, 0x28, 0x3D)
        
    def get_calibration_status(self):
        '''!@brief
            @details       A method to retrieve the calibration status byte from the IMU and parse it into its individual
                           statuses.
            @return        
        '''        
        self.cal_byte = self.i2c.mem_read(1, 0x28, 0x35)[0]
        mag = self.cal_byte & 0b00000011
        acc = (self.cal_byte & 0b00001100)>>2
        gyr = (self.cal_byte & 0b00110000)>>4
        sys = (self.cal_byte & 0b11000000)>>6
        return((mag, acc, gyr, sys))
    
    def get_calibration_coefficients (self, calbuf):
        '''!@brief          Returns calibration coefficients
            @details        A method to retrieve the calibration coefficients from the IMU as an array of packed binary
                            data.
            @param calbuf   array of calibration coefficients
            @return         Returns a new array of calibration coefficients
        '''        
        self.i2c.mem_read(calbuf, 0x28, 0x55)
        return calbuf
    
    def set_calibration_coefficients(self, setbuf):
        '''!@brief           Sets calibration coefficients
            @details         A method to write calibration coefficients back to the IMU from pre-recorded packed binary
                             data.
            @param setbuf    array of calibration coefficients
        '''
        self.i2c.mem_write(setbuf, 0x28, 0x55)

    def get_euler_angles (self):
        '''!@brief           returns Euler angles
            @details         A method to read Euler angles from the IMU to use as measurements for feedback.
            @return          tuple of euler angles
        '''
        self.i2c.mem_read(self.euler_buffer, 0x28, 0x1A)
        zEuler = (self.euler_buffer[1]<<8)|self.euler_buffer[0]
        yEuler = (self.euler_buffer[3]<<8)|self.euler_buffer[2]
        xEuler = (self.euler_buffer[5]<<8)|self.euler_buffer[4]
        if zEuler > 32767:
            zEuler -= 65536
        if yEuler > 32767:
            yEuler -= 65536
        if xEuler > 32767:
            xEuler -= 65536
        return (-zEuler/16,-yEuler/16,-xEuler/16)
        
    def get_omega (self):
        '''!@brief           returns gyro values
            @details         A method to read angular velocity from the IMU to use as measurements for feedback.
            @return          tuple of gyro values
        '''        
        self.i2c.mem_read(self.velocity_buffer, 0x28, 0x14)
        xGyro = (self.velocity_buffer[1]<<8)|self.velocity_buffer[0]
        yGyro = (self.velocity_buffer[3]<<8)|self.velocity_buffer[2]
        zGyro = (self.velocity_buffer[5]<<8)|self.velocity_buffer[4]
        if xGyro > 32767:
            xGyro -= 65536
        if yGyro > 32767:
            yGyro -= 65536
        if zGyro > 32767:
            zGyro -= 65536
        return (xGyro/16, yGyro/16, zGyro/16)