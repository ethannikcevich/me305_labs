'''!@file                   touchpad.py
    @brief                  Contains the touchpad Class and its functions to find ball position and velocity
    @details                The touchpad Class is used to determine if a ball is in contact with the touchpad and its x, y position.
                            This must scan all three x, y, and z values in less than 1500 microseconds.
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   3/17/22
'''

from pyb import Pin, ADC
import micropython
from array import array

class Touchpad:
    '''!@brief    The touchpad class instantiates a touchpad, and returns the x, y, and z values when called.
        @details  A touch panel (touchpad) is situated on the ball balancing platform. This class creates a touchpad object which can be used to find the position of the ball.
    '''

    @micropython.native
    def __init__(self, pinYm, pinXm, pinYp, pinXp, width, length):
        '''!@brief        Initiates a touchpad object
            @details      assigns attributes for all pins and necessary values
            @param pinYm  The MCU pin corresponding to the negative y side of the touchpad
            @param pinXm  The MCU pin corresponding to the negative x side of the touchpad
            @param pinYp  The MCU pin corresponding to the positive y side of the touchpad
            @param pinXp  The MCU pin corresponding to the positive x side of the touchpad
            @param width  The width (x-direction) of the touchpad in mm 
            @param length The length (y-direction) of the touchpad in mm
        '''
        ## @brief   The MCU pin corresponding to the negative y side of the touchpad
        #  @details Configure pinYm to input for finding the x position, low for finding the y position, and measure for the z value.
        self.pinYm = pinYm
        ## @brief   The MCU pin corresponding to the negative x side of the touchpad
        #  @details Configure pinXm to low for finding the x position and z value, and float for finding the y position.
        self.pinXm = pinXm
        ## @brief   The MCU pin corresponding to the positive y side of the touchpad
        #  @details Measure for finding the x position, configure to high for finding the y position, and high for finding the z value.
        self.pinYp = pinYp
        ## @brief   The MCU pin corresponding to the positive x side of the touchpad
        #  @details Configfure pinXp to high for finding the x position and measure for finding the y position.
        self.pinXp = pinXp
        
        ## @brief   Configure the pin for output
        #  @details Used to configure any of the MCU pins to output as needed to retrieve x, y, or z.
        self.OUT = Pin.OUT_PP
        ## @brief   Configure the pin for input
        #  @details Used to configure any of the MCU pins to input as needed to retrieve x, y, or z.
        self.IN = Pin.IN
        
        #It is faster to assign numbers to vars and use them as shown:
            
        ## @brief   An attribute storing the scalar number 4096.
        #  @details The number 4096 is used in the equation to find the x and y positons on the touchpad.
        self.scalar = micropython.const(4096)
        ## @brief   the width of the touchpad divided by the scalar.
        #  @details Although the scalar is technically applied to the ADC value, since the ADC is multiplied by the width, this method works.
        self.scale_width = width/self.scalar
        ## @brief   the length of the touchpad divided by the scalar.
        #  @details Although the scalar is technically applied to the ADC value, since the ADC is multiplied by the length, this method works.
        self.scale_length = length/self.scalar
        ## @brief   An attribute storing the scalar number 2.
        #  @details The number 2 is used in the equation to find the x and y positons on the touchpad.
        self.two = micropython.const(2)
        ## @brief   touchpad center x-value
        #  @details The length from the edge to the center of the touchpad in the x-direction
        self.center_width = width/self.two
        ## @brief   touchpad center y-value
        #  @details The length from the edge to the center of the touchpad in the y-direction
        self.center_length = length/self.two
        
        ## @brief   Coupled linear correction value Kxx
        #  @details Accounts for rotation in the xADC measurement
        self.K_xx = 1
        ## @brief   Coupled linear correction value Kxy
        #  @details Accounts for shear in the yADC measurement
        self.K_xy = 0
        ## @brief   Coupled linear correction value Kyx
        #  @details Accounts for shear in the xADC measurement
        self.K_yx = 0
        ## @brief   Coupled linear correction value Kyy
        #  @details Accounts for rotation in the yADC measurement
        self.K_yy = 1
        ## @brief   Offset in the x-direction
        #  @details Used to correct the x output's offset position
        self.x_o = 0
        ## @brief   Offset in the y-direction
        #  @details Used to correct the y output's offset position
        self.y_o = 0
        ## @brief   the array for ADC values
        #  @details ADC values will be stored here throughout data collection
        self.array = array('h', 25*[0])
        ## @brief   frequency
        #  @details frequency at which ADC.read_timed() will read a sample
        self.frequency = micropython.const(200000)
        ## @brief   The number of items in the array
        #  @details Shortcut, make an attribute with value of 25 for mean calculations
        self.size = micropython.const(25)
        
    @micropython.native
    def get_X(self):        
        '''!@brief      Find the x coordinate value of the item in contact 
            @details    By configuring pins appropriately, the touchpad returns the x coordinate.
            @return     x coordinate of item in contact
        '''
        xp = Pin(self.pinXp, self.OUT)
        xp.high()
        xm = Pin(self.pinXm, self.OUT)
        xm.low()
        yp = Pin(self.pinYp, self.IN)
        ym = ADC(Pin(self.pinYm))
        ym.read_timed(self.array, self.frequency)
        ym_avg = sum(self.array)/self.size
        
        ## @brief   ADC measurement in x-direction
        #  @details The uncorrected x measurement of the item in contact with the touchpad
        self.xADC = ym_avg*self.scale_width - self.center_width
        return self.xADC
        
        
    @micropython.native
    def get_Y(self):
        '''!@brief      Find the y coordinate value of the item in contact 
            @details    By configuring pins appropriately, the touchpad returns the y coordinate.
            @return     y coordinate of item in contact
        '''
        yp = Pin(self.pinYp, self.OUT)
        yp.high()
        ym = Pin(self.pinYm, self.OUT)
        ym.low()
        xp = Pin(self.pinXp, self.IN)
        xm = ADC(Pin(self.pinXm))
        xm.read_timed(self.array, self.frequency)
        xm_avg = sum(self.array)/self.size
        
        ## @brief   ADC measurement in y-direction
        #  @details The uncorrected y measurement of the item in contact with the touchpad
        self.yADC = xm_avg*self.scale_length - self.center_length
        return self.yADC
        
        
        
    @micropython.native
    def get_Z(self):
        '''!@brief      Find if an item is in contact 
            @details    By configuring pins appropriately, determines if an item is in contact.
            @return     z is a boolean to determine if the touchpad is being pressed
        '''
        
        yp = Pin(self.pinYp, self.OUT)
        yp.high()
        xm = Pin(self.pinXm, self.OUT)
        xm.low()
        xp = Pin(self.pinXp, self.IN)
        ym = ADC(Pin(self.pinYm))
        
        # Might need to change this from 4100 (lower)
        if ym.read < 4000:
            ## @brief   A boolean to determine if the touchpad is being pressed
            #  @details If z is 1, then there is contact. 0 means no contact
            self.z = 1
        else:
            self.z = 0
        return self.z

    @micropython.native
    def Read(self):
        '''!@brief    Find x, y, and z values
            @details  Calls get_X, get_Y, and get_Z, performs linear correction, and returns a tuple.
            @return   Tuple of X, Y, and Z values
        '''
        get_x = self.get_X()
        get_y = self.get_Y()
        self.get_Z()
        ## @brief   The corrected x-distance in mm
        #  @details x-distance after calibration
        self.x = self.K_xx*get_x + self.K_xy*get_y + self.x_o
        ## @brief   The corrected x-distance in mm
        #  @details x-distance after calibration
        self.y = self.K_yx*get_x + self.K_yy*get_y + self.y_o
        return(self.x, self.y, self.z)
        
    @micropython.native
    def calibrate(self, K_xx, K_xy, x_o, K_yx, K_yy, y_o):
        '''! @brief   Write linear correction values to the touchpad object as needed.
             @details Allows for the touchpad to be callibrated by having linear correction values written to it after being created.
        '''
        self.K_xx = K_xx
        self.K_xy = K_xy
        self.x_o = x_o
        self.K_yx = K_yx
        self.K_yy = K_yy
        self.y_o = y_o
        
if __name__ == '__main__':
    import utime
    x = Touchpad(Pin.cpu.A0, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A7, 176, 100)
    i = 0
    start = utime.ticks_us()
    while i < 100:
        x.Read()
        i += 1
    finish = utime.ticks_us()
    time = utime.ticks_diff(finish, start)/100
    print(f'Scan for x, y, and z took: {time} microseconds')