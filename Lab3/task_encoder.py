'''!@file                   task_encoder.py
    @brief                  Retrieves and passes values to encoder.py
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/13/22
'''

import encoder
import utime

class Task_Encoder:
    '''!@brief A class for interacting with the encoder
    '''
    def __init__(self, period, timNum, pin1, pin2, encoderID):
        '''!@brief Constructs a Task_Encoder class, using the specified values.
            @param period
            @param timNum
            @param pin1
            @param pin2
            @param encoderID   Encoder 1 or Encoder 2
        ''' 
        self.Period = period
        self.pin1 = pin1
        self.pin2 = pin2
        
        self.enc = encoder.Encoder(timNum, 65535, self.pin1, self.pin2)
        self.next_time = utime.ticks_add(utime.ticks_ms(), self.Period)
        
        self.encoderID = encoderID

        
    def run(self, zFlag):
        '''!@brief Zeros the encoder
            @param zFlag
            @return enc.get_position
            @return enc.get_delta
            @return enc.get_velocity
        '''
        if(utime.ticks_ms() >= self.next_time):
           
            if zFlag.read() == self.encoderID:
                self.enc.zero(0)
                zFlag.write(0)
                
            self.enc.update()
            self.next_time = utime.ticks_add(utime.ticks_ms(), self.Period)

            
        return (self.enc.get_position(), self.enc.get_delta(), self.enc.get_velocity())