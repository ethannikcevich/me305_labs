'''!@file       driver.py
    @brief      DRV8847 and motor class.
    @details    Establishes functions to be used by task_driver.py 
                to interact with the motors
    @author     Ethan Nikcevich
    @author     Johnathan Dietz
    @date       2/13/22
'''

import pyb, utime

class DRV8847:
    '''!@brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or more objects of the
                    Motor class which can be used to perform motor
                    control.
                    
                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''
    
    def __init__(self, sleep1, sleep2, fault):
        '''!@brief      Initializes and returns a DRV8847 object.
        '''
        self.sleep_pin = pyb.Pin(sleep1, sleep2)
        self.fault = fault
        self.FaultInt = pyb.ExtInt(self.fault, mode=pyb.ExtInt.IRQ_FALLING, pull=pyb.Pin.PULL_NONE, callback=self.fault_cb)
        pass
    
    def enable(self):
        '''!@brief      Brings the DRV8847 out of sleep mode.
        '''
        self.faultCheck = False
        self.sleep_pin.high()
        utime.sleep_us(30)
        self.faultCheck =  True
        pass
    
    def disable(self):
       '''!@brief      Puts the DRV8847 in sleep mode.
       '''
       self.sleep_pin.low()
       print('Motors Disabled')
       pass 
    
    def fault_cb(self, IRQ_src):
        '''!@brief      Callback function to run on fault condition.
            @param      IRQ_src The source of the interrupt request.
        '''
        if self.faultCheck == True:
            self.sleep_pin.low()
            print('fault detected, press c to clear')
        pass
    
    def motor(self, pin1, pin2, ch1, ch2):
        '''!@brief      Initializes and returns a motor object associated with the DRV8847.
            @return     An object of class Motor
        '''
        return Motor(pin1, ch1, pin2, ch2)
    
class Motor:
    '''!@brief      A motor class for one channel of the DRV8847.
        @details    Objects of this class can be used to apply PWM to a given
                    DC motor.
    '''
    def __init__(self, pin1, ch1, pin2, ch2):
        '''!@brief      Initializes and returns a motor object associated with the DRV8847.
            @details    Objects of this class should not be instantiated
                        directly. Instead create a DRV8847 object and use
                        that to create Motor objects using the method
                        DRV8847.motor().
        '''
        self.tim3 = pyb.Timer(3, freq = 25000)
        self.t3ch1 = self.tim3.channel(ch1, pyb.Timer.PWM, pin=pin1)
        self.t3ch2 = self.tim3.channel(ch2, pyb.Timer.PWM, pin=pin2)
        pass
    
    def set_duty(self, duty):
        '''!@brief      Set the PWM duty cycle for the motor channel.
            @details    This method sets the duty cycle to be sent
                        to the motor to the given level. Positive values
                        cause effort in one direction, negative values
                        in the opposite direction.
            @param      duty    A signed number holding the duty
                                cycle of the PWM signal sent to the motor
        '''
        if duty > 0:
            self.t3ch1.pulse_width_percent(abs(duty))
            self.t3ch2.pulse_width_percent(0)
        elif duty < 0:
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(abs(duty))
        elif duty == 0:
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(0)
        pass
    
if __name__ =='__main__':
        # Adjust the following code to write a test program for your motor class. Any
        # code within the if __name__ == '__main__' block will only run when the
        # script is executed as a standalone program. If the script is imported as
        # a module the code block will not run.
        
        
        # Create a motor driver object and two motor objects. You will need to
        # modify the code to facilitate passing in the pins and timer objects needed
        # to run the motors.
        mot_drv = DRV8847( pyb.Pin.cpu.A15, pyb.Pin.OUT_PP, pyb.Pin.cpu.B2 )
        motor1 = mot_drv.motor( pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2)
        motor2 = mot_drv.motor( pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4)
            
        # Enable the motor driver
        mot_drv.enable()
            
        # Set the duty cycle of the first motor to 40 percent and the duty cycle of
        # the second motor to 60 percent
        motor1.set_duty(-100)
        motor2.set_duty(-100)