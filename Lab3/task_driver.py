'''!@file    task_driver.py
    @brief   Task used for controlling motors.
    @details Uses provided information to interact with driver.py
             to control the speed of the motors.
    @author  Ethan Nikcevich
    @author  Johnathan Dietz
    @date    2/13/22
'''

import driver
import pyb

class Task_Driver:
    '''!@brief    A task for interacting with the motor driver.
    '''

    def __init__(self):
        '''!@brief   Constructs a Task_Driver class.
        '''
        self.mot_drv = driver.DRV8847( pyb.Pin.cpu.A15, pyb.Pin.OUT_PP, pyb.Pin.cpu.B2 )
        self.motor1 = self.mot_drv.motor( pyb.Pin.cpu.B4, pyb.Pin.cpu.B5, 1, 2)
        self.motor2 = self.mot_drv.motor( pyb.Pin.cpu.B0, pyb.Pin.cpu.B1, 3, 4)
        self.mot_drv.enable()
        
    def run(self, motor1, motor2, clear):
        '''!@brief Called to control motor speeds.
            @param motor1
            @param motor2
            @param clear
        '''        
        # set motors to specified duty cycle
        self.motor1.set_duty(motor1.read())
        self.motor2.set_duty(motor2.read())
        
        # clear fault condition
        if clear.read():
            self.mot_drv.enable()
            clear.write(False)