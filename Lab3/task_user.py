'''!@file                   task_user.py
    @brief                  File for all user interactions
    @details                Interacts with the user by responding to keyboard 
                            inputs
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/13/22
'''

import utime
import pyb
import array

class Task_User:
    '''!@brief    A task for interacting with the user.
    '''
    
    def __init__(self, period, collectTime):
        '''!@brief Sets vars to preliminary values and calls hardware 
            @param period    
            @param collectTime
        ''' 
        self.serport = pyb.USB_VCP()
        self.period = period
        self.collectTime = collectTime
        self.nextTime = 0
        self.collectEncoderData1 = False
        self.collectEncoderData2 = False
        self.cells = 30000/int(self.collectTime)
        # preallocate array sizes for efficiency
        self.timeArray = array.array( 'f', [self.cells]*0)
        self.positionArray = array.array( 'f', [self.cells]*0)
        self.deltaArray = array.array( 'f', [self.cells]*0)
        # vars to track which collection pressing s should interrupt
        self.g_pressed = False
        self.t_pressed = False
        # self.motor_stable = False
        # arrays for testing interface
        self.dutyArray = array.array( 'f', [1]*0)
        self.velocityArray = array.array( 'f', [1]*0)
        # need a counter to wait for the motor to get up to speed during test interface
        self.c = 0
        # To check if it is time to print the gather data
        self.gPrint = False
        self.index = 0
        self.counter = 0
        self.startTime = utime.ticks_ms()
      
        
              
    def run(self, encoderData1, encoderData2, motor1, motor2, zFlag, clear):
        '''!@brief   Main method that controls user interface.
            @details Interprets all user commands, and collects motor data
            @param   encoderData1
            @param   encoderData2
            @param   motor1
            @param   motor2
            @param   zFlag
            @param   clear
        ''' 
        self.currentTime = utime.ticks_diff(utime.ticks_ms(), self.startTime)
        
        if self.serport.any():
            charIn= self.serport.read(1).decode()
                              
            if charIn== 'z':
                zFlag.write(1)
                print('Position of motor 1:  0 ')
                    
            elif charIn== 'Z':
                zFlag.write(2)
                print('Position of motor 2:  0 ')
                    
            elif charIn== 'p':
                print("Position of motor 1: ", encoderData1[0])
                    
            elif charIn== 'P':
                print("Position of motor 2: ", encoderData2[0])
                    
            elif charIn== 'd':
                print("Delta of motor 1: ", encoderData1[1], "ticks")
                
            elif charIn == 'D':
                print("Delta of motor 2: ", encoderData2[1], "ticks")
                    
            elif charIn == 'v':
                print("Velocity of motor 1: ", encoderData1[2], "rad/s")
                
            elif charIn == 'V':
                print("Velocity of motor 2: ", encoderData2[2], "rad/s")
                    
            elif charIn == 'm':
                print('Enter desired motor 1 duty cycle:')
                dutyInput = ''
                while True:
                    if self.serport.any() == True:
                        
                        char = self.serport.read(1).decode()
                        if not char.isalpha():
                            self.serport.write(char)
                            if char.isdigit() ==True:
                                dutyInput += char
                            elif char == '-':
                                if dutyInput == '':
                                    dutyInput += char
                            elif char == '\x7F':
                                if dutyInput != '':    
                                    newDutyInput = dutyInput
                                    dutyInput = newDutyInput[:-1]
                            elif char == '\r' or char == '\n':
                                if float(dutyInput) < -100:
                                    self.motorSpeed1 = -100
                                    motor1.write(self.motorSpeed1)
                                    print("duty cycle of motor 1: ", motor1.read())
                                    break
                                elif float(dutyInput) > 100:
                                    self.motorSpeed1 = 100
                                    motor1.write(self.motorSpeed1)
                                    print("duty cycle of motor 1: ", motor1.read())
                                    break
                                else:
                                    self.motorSpeed1 = float(dutyInput)
                                    motor1.write(self.motorSpeed1)
                                    print("duty cycle of motor 1: ", motor1.read())
                                    break
                    
            elif charIn == 'M':
                print('Enter desired motor 2 duty cycle:')
                dutyInput = ''
                while True:
                    if self.serport.any() == True:
                        char = self.serport.read(1).decode()
                        if not char.isalpha():
                            self.serport.write(char)
                            if char.isdigit() ==True:
                                dutyInput += char
                            elif char == '-':
                                if dutyInput == '':
                                    dutyInput += char
                            elif char == '\x7F':
                                if dutyInput != '':    
                                    newDutyInput = dutyInput
                                    dutyInput = newDutyInput[:-1]
                            elif char == '\r' or char == '\n':
                                if float(dutyInput) < -100:
                                    self.motorSpeed2 = -100
                                    motor2.write(self.motorSpeed2)
                                    print("duty cycle of motor 2: ", motor2.read())
                                    break
                                elif float(dutyInput) > 100:
                                    self.motorSpeed2 = 100
                                    motor2.write(self.motorSpeed2)
                                    print("duty cycle of motor 2: ", motor2.read())
                                    break
                                else:
                                    self.motorSpeed2 = float(dutyInput)
                                    motor2.write(self.motorSpeed2)
                                    print("duty cycle of motor 2: ", motor2.read())
                                    break
                    
            elif charIn in {'c', 'C'}:
                clear.write(True)
                print('cleared fault')
                
            elif charIn in {'s', 'S'}:
                self.collectEncoderData1 = False
                self.collectEncoderData2 = False
                print(' Data collection was stopped ')
                self.gPrint = True
                
            elif charIn == 'g':
                self.g_pressed = True
                self.nextTime = self.collectTime
                self.collectEncoderData1 = True
                print('collecting data encoder 1...')
                self.startTime = utime.ticks_ms()
                self.currentTime = 0
                self.counter = 0
                self.timeArray = array.array('f', [0]*300)
                self.positionArray = array.array('f', [0]*300)
                self.deltaArray = array.array('f', [0]*300)
                    
            elif charIn == 'G':
                self.g_pressed = True
                self.nextTime = self.collectTime
                self.collectEncoderData2 = True
                print('collecting data encoder 2...')
                self.startTime = utime.ticks_ms()
                self.currentTime = 0
                self.counter = 0
                self.timeArray = array.array('f', [0]*300)
                self.positionArray = array.array('f', [0]*300)
                self.deltaArray = array.array('f', [0]*300)
                    
            elif charIn in {'t', 'T'}:
                print('Enter desired motor 1 duty cycle or press s to exit the testing interface:')
                self.t_pressed = True
                self.collectEncoderData1 = True
                dutyInput = '' 
                while True:
                        if self.serport.any() == True:
                            char = self.serport.read(1).decode()
                            self.serport.write(char)
                            if char.isdigit() == True:
                                dutyInput += char
                            elif char == '-':
                                if dutyInput == '':
                                    dutyInput += char
                            elif char == '\x7F':
                                if dutyInput != '':    
                                    newDutyInput = dutyInput
                                    dutyInput = newDutyInput[:-1]
                            elif char == '\r' or char == '\n':
                                if float(dutyInput) < -100:
                                    self.c = 1
                                    self.motorSpeed1 = -100
                                    motor1.write(self.motorSpeed1)
                                    print("duty cycle of motor 1: ", motor1.read())
                                    self.dutyArray.append(motor1.read())
                                    break
                                elif float(dutyInput) > 100:
                                    self.c = 1
                                    self.motorSpeed1 = 100
                                    motor1.write(self.motorSpeed1)
                                    print("duty cycle of motor 1: ", motor1.read())
                                    self.dutyArray.append(motor1.read())
                                    break
                                else:
                                    self.c = 1
                                    self.motorSpeed1 = float(dutyInput)
                                    motor1.write(self.motorSpeed1)
                                    print("duty cycle of motor 1: ", motor1.read())
                                    self.dutyArray.append(motor1.read())
                                    break
                                
            elif charIn in {'h', 'H'}:
                self.help_command()
                
        if self.c > 0:
            self.c += 1
        if self.c > 1500:
            print("motor speed is ", encoderData1[2])
            self.velocityArray.append(encoderData1[2])
            self.c = 0
            # self.motor_stable = True
            print('Enter another desired motor 1 duty cycle or press s to exit the testing interface:')
            self.t_pressed = True
            self.collectEncoderData1 = True

            dutyInput = ''
            
            while True:
                if self.serport.any() == True:
                    char = self.serport.read(1).decode()
                    self.serport.write(char)
                    if char.isdigit() == True:
                        dutyInput += char
                    elif char == '-':
                        if dutyInput == '':
                            dutyInput += char
                    elif char == '\x7F':
                        if dutyInput != '':    
                            newDutyInput = dutyInput
                            dutyInput = newDutyInput[:-1]
                    elif char == '\r' or char == '\n':
                        if float(dutyInput) < -100:
                            self.c = 1
                            self.motorSpeed1 = -100
                            motor1.write(self.motorSpeed1)
                            print("duty cycle of motor 1: ", motor1.read())
                            self.dutyArray.append(motor1.read())
                            break
                        elif float(dutyInput) > 100:
                            self.c = 1
                            self.motorSpeed1 = 100
                            motor1.write(self.motorSpeed1)
                            print("duty cycle of motor 1: ", motor1.read())
                            self.dutyArray.append(motor1.read())
                            break
                        else:
                            self.c = 1
                            self.motorSpeed1 = float(dutyInput)
                            motor1.write(self.motorSpeed1)
                            print("duty cycle of motor 1: ", motor1.read())
                            self.dutyArray.append(motor1.read())
                            break
                    elif char == 's' or char == 'S':
                        self.collectEncoderData1 = False
                        self.collectEncoderData2 = False
                        print(' Data collection was stopped ')
                        self.gPrint = True
                        break
            
        # this is where the testing interface (t) data will be printed:
        if not self.collectEncoderData1 and self.t_pressed: # and self.motor_stable:
            motor1.write(0)
            print("----Start Data----")
            print("Duty       Speed")
            for l in range(len(self.dutyArray)):
                print(self.dutyArray[l],',  ', self.velocityArray[l])
            print("----End Data----")
            self.dutyArray = array.array( 'f', [])
            self.velocityArray = array.array( 'f', [])
            self.collectEncoderData1 = False
            self.t_pressed = False
            #self.motor_stable = False
            pass
                    
        if self.g_pressed and self.collectEncoderData1 and self.currentTime >= self.nextTime:
            self.array_time = float(self.currentTime)/100
            self.timeArray[self.counter] = round(self.array_time,1)/10
            self.positionArray[self.counter] = (encoderData1[0])
            self.deltaArray[self.counter] = (encoderData1[1])
            self.nextTime += self.collectTime
            self.counter += 1
            
            if self.currentTime >= 30000:
                self.collectEncoderData1 = False
                self.gPrint = True
                
        if self.g_pressed and self.collectEncoderData2 and self.currentTime >= self.nextTime:
           
            self.array_time = float(self.currentTime)/100
            self.timeArray[self.counter] = round(self.array_time,1)/10
            self.positionArray[self.counter] = (encoderData2[0])
            self.deltaArray[self.counter] = (encoderData2[1])
            self.nextTime += self.collectTime
            self.counter +=1
            
            if self.currentTime >= 30000:
                self.collectEncoderData2 = False 
                self.gPrint = True
                
        if self.g_pressed and self.gPrint == True and self.index < 300:
            if self.timeArray[self.index] > 0:
                print(self.timeArray[self.index],'s', self.positionArray[self.index],'rad', self.deltaArray[self.index],'rad/s')
                self.index += 1
            else:
                self.g_pressed = False
            
        else:
            self.gPrint = False
            self.index = 0
     
    def help_command(self):
        '''!@brief Command List  
        ''' 
        print('+----------------------------------------------+')
        print('| Use the following commands:                  |')
        print('| z : Zero the position of encoder 1           |')
        print('| Z : Zero the position of encoder 2           |')
        print('| p : Print the position of encoder 1          |')
        print('| P : Print the position of encoder 2          |')
        print('| d : Print the delta of encoder 1             |')
        print('| D : Print the delta of encoder 2             |')
        print('| v : Print the velocity of encoder 1          |')
        print('| V : Print the velocity of encoder 2          |')
        print('| m : Enter a duty cycle for motor 1           |')
        print('| M : Enter a duty cycle for motor 2           |')
        print('| c or C : Clear a fault condition             |')
        print('| g : Collect data from encoder 1 for 30 secs  |')
        print('| G : Collect data from encoder 2 for 30 secs  |')
        print('| t or T : Begin testing interface             |')
        print('| s or S : End data collection prematurely     |')
        print('| h or H : Print this help message             |')
        print('| Ctrl-C : Terminate Program                   |')
        print('+----------------------------------------------+')  
        