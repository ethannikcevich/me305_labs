'''!@file                   main.py
    @brief                  Main file to initiate the program
    @details                Establishes flags for state transitions and calls 
                            the User and Encoder tasks
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/13/22
'''

import shares
import task_encoder
import task_user
import task_driver
import pyb

# Creating variables for the necessary pins
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

# A variable for zeroing the encoder
zFlag = shares.Share(0)
clear = shares.Share(False)

# There are two different encoders
motor1 = shares.Share(0)
motor2 = shares.Share(0)

# Timing delay for tasks
period = 5 

if __name__ =='__main__':
    # Initiating Encoder Task for Encoder 1
    encoder1 = task_encoder.Task_Encoder(period, 4, pinB6, pinB7, 1)
    # Initiating Encoder Task for Encoder 2
    encoder2 = task_encoder.Task_Encoder(period, 8, pinC6, pinC7, 2)
    #Initiating Driver Task
    driver = task_driver.Task_Driver()
    # Initiating User Task
    user = task_user.Task_User(period, 100)
    # Printing Help Command
    user.help_command()
    
    while True:
        try:
            # Run the tasks on a loop
            driver.run(motor1, motor2, clear)
            # Update Encoder 1 Data
            encoderData1 = encoder1.run(zFlag)
            # Update Encoder 2 Data
            encoderData2 = encoder2.run(zFlag)
            user.run(encoderData1, encoderData2, motor1, motor2, zFlag, clear)
            
            
        except KeyboardInterrupt:
            print('Program Terminating')
            break