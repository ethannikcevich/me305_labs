# -*- coding: utf-8 -*-
import time
import pyb
import math

'''!@file                    Lab0x01.py
    @brief                   This file cycles through LED pulse patterns with 
                             the press of a button.
    @details                 cycle through various LED pulse patterns by 
                             clicking the blue user button on the Nucleo. 
                             There are three patterns: square wave, sine wave, 
                             and saw wave.
    @author                  Ethan Nikcevich
    @author                  Johnathan Dietz
    @date                    January 19, 2022
'''

def onButtonPressFCN(IRQ_src):
    '''!@brief           Button Press Trigger
        @details         Upon receiving an interrupt request (pressed button), 
                         makes Button_Pressed = True
        @param IRQ_src   Interrupt request from button
    '''
    global Button_Pressed 
    Button_Pressed = True

def SquareWave(timeSinceStart):
    '''!@brief                  Square wave function
        @details                Flashes the LED in a square wave pattern, 
                                with a period of 1 second
        @param timeSinceStart   Time that waveform begins
    '''
    if timeSinceStart % 1000 < 500:   #Loop alternates output over .5 seconds
        sqrvar = 100
    else:
        sqrvar = 0
    return sqrvar

def SineWave(timeSinceStart):
    '''!@brief                  Sine wave function
        @details                Flashes the LED in a Sine wave pattern, 
                                with a period of 10 seconds
        @param timeSinceStart   Time that waveform begins
    '''
    sine = 0.5*100*math.sin(math.pi/5000*time.ticks_ms())+0.5*100
    return sine                        #shifts sine wave and returns variable

def SawWave(timeSinceStart):
    '''!@brief                  Saw wave function
        @details                Flashes the LED in a Saw wave pattern, 
                                with a period of 1 second
        @param timeSinceStart   Time that waveform begins
    '''
    sawvar = timeSinceStart%1000 * 0.1   #Counts time
    return sawvar
        

if __name__ == '__main__':
    state = 0 
    sqrvar = 0
    sawvar = 0                           #variable declarations
    print('3 LED pulse patterns can be toggled by pressing the blue button. Dont press the black button.')
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)   #pin assignments for button
    pinA5 = pyb.Pin(pyb.Pin.cpu.A5)     #pin assignment for LED
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    tim2 = pyb.Timer(2, freq=20000)                     #configuring timer
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    Button_Pressed = False
 
    while(True):
        try:

            if (state == 0):                       #initial state
                
                if(Button_Pressed == True):        #checking button status to change state
                    state = 1
                    Button_Pressed = False
                    SquareStart = time.ticks_ms()  #sets time for next state
                    print("running state 1 (sqaure wave)")
                    
            elif (state == 1):                     #State 1: Square wave
                
                timer = time.ticks_diff(time.ticks_ms(), SquareStart)      #sets timer for function
                t2ch1.pulse_width_percent(SquareWave(timer))               #sets LED brightness using output from SquareWave()
                    
                if(Button_Pressed == True):       #checking button status to change state
                    state = 2    
                    Button_Pressed = False
                    SineStart = time.ticks_ms()   #sets time for next state
                    print("running state 2 (sine wave)")
            
            elif (state == 2):                    #State 2: Sine Wave
                
                timer = time.ticks_diff(time.ticks_ms(), SineStart)      #sets timer for function
                t2ch1.pulse_width_percent(SineWave(timer))               #sets LED brightness using output from SineWave()
                
                if(Button_Pressed == True):        #checking button status to change state
                    state = 3    
                    Button_Pressed = False
                    SawStart = time.ticks_ms()     #sets time for next state
                    print("running state 3 (saw wave)")
            
            elif (state == 3):                     #State 3: Saw Wave
                
                timer = time.ticks_diff(time.ticks_ms(), SawStart)      #sets timer for function
                t2ch1.pulse_width_percent(SawWave(timer))               #sets LED brightness using outut from SawWave() 
                
                if(Button_Pressed == True):        #checking button status to change state
                    state = 1    
                    Button_Pressed = False
                    SquareStart = time.ticks_ms()  #sets time for next state
                    print("running state 1 (sqaure wave)")

            time.sleep(0.1)           #delay so the program doesn't run too quickly

        except KeyboardInterrupt:     #method to exit the program
            break
    print("end program ")