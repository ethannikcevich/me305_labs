'''!@file       DRV8847.py
    @brief      enables and disables the motor driver. Also triggers faults
    @details    Establishes functions to be used by task_driver.py 
                to interact with the motors
    @author     Ethan Nikcevich
    @author     Johnathan Dietz
    @date       2/13/22
'''
import pyb, time, motor

class DRV8847:
    '''!@brief      A motor driver class for the DRV8847 from TI.
        @details    Objects of this class can be used to configure the DRV8847
                    motor driver and to create one or more objects of the
                    Motor class which can be used to perform motor
                    control.
                    
                    Refer to the DRV8847 datasheet here:
                    https://www.ti.com/lit/ds/symlink/drv8847.pdf
    '''

    def __init__(self, tim, sleep_pin, fault_pin):
        '''!@brief            Initializes and returns a DRV8847 object.
            @details          Creates a motor driver for controlling the motors
            @param tim        Decides which timer will be used
            @param sleep_pin  The pin for enabling and disabling the motor
            @param fault_pin  The pin for recognizing faults
        '''
        self.tim = tim
        self.sleep_pin = sleep_pin
        self.fault_pin = fault_pin
        self.fault = pyb.ExtInt(self.fault_pin, mode = pyb.ExtInt.IRQ_FALLING, pull = pyb.Pin.PULL_NONE, callback = self.fault_cb)

    def enable(self):
        '''!@brief      Brings the DRV8847 out of sleep mode.
            @details    Enables the DRV8847 by setting the sleep pin to high
        '''
        self.fault.disable()
        self.sleep_pin.high()
        time.sleep_us(30)
        self.fault.enable()
        
    def disable(self):
        '''!@brief      Puts the DRV8847 in sleep mode.
            @details    Disables the DRV8847 by setting the sleep pin to low
        '''
        print('Motors disabled')
        self.sleep_pin.low()
    
    def fault_cb(self, IRQ_src):
        '''!@brief          Callback function to run on fault condition.
            @details        When a fault is detected, the motors are disabled.
            @param IRQ_src  The source of the interrupt request.
        '''
        self.disable()
        print('fault detected, press c to clear')

    def motor(self, pin1, pin2, ch1, ch2):
        '''!@brief           Initializes and returns a motor object associated with the DRV8847.
            @details         By creating motor objects in this way, a single DRV8847 can command multiple motors
            @param pin1      First pin used by motor
            @param pin2      Second pin used by motor
            @param ch1       First channel used by motor
            @param ch2       Second channel used by motor
            @return Motor()  An object of class Motor
        '''
        return motor.Motor(self.tim, pin1, pin2, ch1, ch2)