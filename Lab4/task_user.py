'''!@file                   task_user.py
    @brief                  File for all user interactions
    @details                Interacts with the user by responding to keyboard 
                            inputs
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/19/22
'''

from time import ticks_us, ticks_add, ticks_diff
import pyb
import array
import micropython
import gc

# In order to differentiate between the various instances of inputting a number
input_m = micropython.const(1)
input_M = micropython.const(2)
input_t = micropython.const(3)
input_y = micropython.const(4)
input_Kp = micropython.const(5)
input_Ki = micropython.const(6)

# States used inside task_user
S0_INIT = micropython.const(0)
S1_CMD = micropython.const (1)
S2_ZERO = micropython.const(2)
S3_GATHER = micropython.const(3)
S4_GATHER_PRINT = micropython.const(4)
S5_NUMBER = micropython.const(5)
S6_TEST = micropython.const(6)
S7_TEST_PRINT = micropython.const(7)
S8_FIND_VELOCITY = micropython.const(8)
S9_CL_GATHER = micropython.const(9)
S10_CL_PRINT = micropython.const(10)

def help_command():
        '''!@brief    Command List
            @details  Prints a list of key inputs available to the user
        ''' 
        print('+---------------------------------------------------+')
        print('| Use the following commands:                       |')
        print('| z or Z : Zero the position of encoder 1           |')
        print('| p or P : Print the position of encoder 1          |')
        print('| d or D : Print the delta of encoder 1             |')
        print('| v or V : Print the velocity of encoder 1          |')
        print('| m : Enter a duty cycle for motor 1                |')
        print('| M : Enter a duty cycle for motor 2                |')
        print('| c or C : Clear a fault condition                  |')
        print('| g or G : Collect data from encoder 1 for 30 secs  |')
        print('| t or T : Begin testing interface                  |')
        print('| s or S : End processes prematurely                |')
        print('| w or W : Toggle closed-loop control for motor 1   |')
        print('| k or K : Enter the gain values for motor 1        |')
        print('| y or Y : Enter a target velocity for motor 1      |')
        print('| r or R : Step response test for motor 1           |')
        print('| h or H : Print this help message                  |')
        print('| Ctrl-C : Terminate Program                        |')
        print('+---------------------------------------------------+')  

def taskUser(taskname, period, zFlag, eFlag, cFlag, encoderData1, duty1, duty2, KpFlag, KiFlag, yFlag, wFlag):
    
    '''!@brief               Main method that controls user interface.
        @details             Interprets all user commands
        @param taskname      To run multiple tasks
        @param period        period of how frequently the task is run
        @param zFlag         Used to transistion to the zero state
        @param eFlag         Used to enable motors
        @param cFlag         Used to clear fault
        @param encoderData1  list of time, position, delta, velocity, and duty encoder data
        @param duty1         duty value for motor 1
        @param duty2         duty value for motor 2
        @param KpFlag        Used to send the Kp value between tasks
        @param KiFlag        Used to send the Ki value between tasks
        @param yFlag         Used to send the reference velocity value between tasks
        @param wFlag         closed loop toggle transition
    '''
    
    # Starting state is S0
    state = S0_INIT
    
    # To create an internal timer
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    # Arrays used for collecting and printing of data
    timeArray = array.array('f', 3001*[0])
    dutyArray = array.array('f', 301*[0])
    gc.collect()
    positionArray = array.array('f', timeArray)
    velocityArray = array.array('f', timeArray)
    # A boolean that influences flowchart if r was pressed
    r_check = False
    # for finding average velocity, create an index
    velocity_index = 0
    # For indexing through times tested
    test_index = 0
    # For indexing through the test arrays when printing
    array_index = 0
    # The current number of data points during collection
    current_size = 0
    # Index used inside print of collected data
    print_index = 0 
    # For user input of numbers
    num_input = ''
    
    serport = pyb.USB_VCP()
    
    while True:
        
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
 
            if state == S0_INIT:
                state = S1_CMD
                
            # State 1 waits for user input to select an action
            elif state == S1_CMD:

                if serport.any():
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'h', 'H'}:
                        help_command() 

                    elif charIn in {'z', 'Z'}:
                        state = S2_ZERO
                        zFlag.write(True)
                    
                    elif charIn in {'p', 'P'}:
                        # The [1] position corresponds to the position of the encoder
                        position = encoderData1.read()[1]
                        print(f'Position of encoder 1: {position:.2f} rad')
                    
                    elif charIn in {'d', 'D'}:
                        # The [2] position corresponds to the delta of the encoder
                        delta = encoderData1.read()[2]
                        print(f'Delta of encoder 1: {delta:.2f} rad')
                        
                    elif charIn in {'v', 'V'}:
                        # The [3] position corresponds to the velocity of the encoder
                        velocity = encoderData1.read()[3]
                        print(f'Velocity of encoder 1: {velocity:.2f} rad/s')
                    
                    elif charIn in {'g', 'G'}:
                        state = S3_GATHER
                        print_index = 0
                        current_size = 0    
                        print('collecting data from encoder 1')
                        
                    elif charIn in {'c', 'C'}:
                        cFlag.write(True)
                        duty1.write(0)
                        duty2.write(0)
                        print('Motors halted. Fault cleared')
                        
                    elif charIn in {'e', 'E'}:
                        eFlag.write(True)
                        duty1.write(0)
                        duty2.write(0)
                        print('Motors Enabled')
                        
                    elif charIn in {'w', 'W'}:
                        if wFlag.read():
                            print('Disabled Closed Loop control')
                            wFlag.write(False)
                        else:
                            print('Enabled Closed Loop control')
                            wFlag.write(True)

                    elif charIn == 'm':
                        # Motors can only be set to a duty cycle if closed loop control is not active
                        if wFlag.read():
                            print('You must disable Closed Loop control to enter a duty cycle')
                        else:
                            state = S5_NUMBER
                            ## button_check is used to determine which command the number input is for.
                            #
                            button_check = input_m
                            print('Enter desired motor 1 duty cycle:')
                        
                    elif charIn == 'M':
                        # Motors can only be set to a duty cycle if closed loop control is not active
                        if wFlag.read():
                            print('You must disable Closed Loop control to enter a duty cycle')
                        else:
                            state = S5_NUMBER
                            button_check = input_M
                            print('Enter desired motor 2 duty cycle:')
                        
                    elif charIn in {'k', 'K'}:
                        state = S5_NUMBER
                        button_check = input_Kp
                        print('Choose a Kp Gain Value for motor 1')
                        
                    elif charIn in {'y', 'Y'}:
                        state = S5_NUMBER
                        button_check = input_y
                        print('Choose a Set Point Velocity for motor 1')
                        
                    elif charIn in {'t', 'T'}:
                        state = S6_TEST
                        testArray = []
                        test_velocityArray = []
                        array_index = 0
                        test_index = 0
                        
                    elif charIn in {'r', 'R'}:
                        state = S5_NUMBER
                        wFlag.write(True)
                        button_check = input_Kp
                        r_check = True
                        print_index = 0
                        current_size = 0
                        print('Choose a Kp Gain Value for motor 1')
               
            # This state is for zeroing the encoder
            elif state == S2_ZERO:
                if not zFlag.read():
                    state = S1_CMD
                    print('Position of encoder 1:  0 ')
                    
            # This state is for collecting data from the encoder into arrays
            elif state == S3_GATHER:
                if current_size <= 3000:
                    # Delta and duty are not needed in this state, but they need to be filled with something.
                    timeArray[current_size], positionArray[current_size], NA, velocityArray[current_size], NA = encoderData1.read()
                    current_size += 1
                    # if user presses s, transition to print state
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn == 's':
                            state = S4_GATHER_PRINT
                    # if 30 seconds have passed, transition to print state
                    if current_size > 3000:
                        state = S4_GATHER_PRINT
            
            # This state is for printing the data gathered from the encoder
            elif state == S4_GATHER_PRINT:
                # Prints line by line until the whole array has been printed
                if print_index < current_size:
                    print(f'{((timeArray[print_index]-timeArray[0])/1000):.2f}, {(positionArray[print_index]):.2f}, {(velocityArray[print_index]):.2f}')
                    print_index += 1
                else:
                    state = S1_CMD
            
            # This state is for user inputs of numbers, used by assigning duty cycles and closed loop operations
            elif state == S5_NUMBER:
                if serport.any():
                    char = serport.read(1).decode()
                    if char.isdigit():
                        num_input += char
                        serport.write(char)
                    # allow - in first spot to create negative numbers
                    elif char == '-':
                        if len(num_input) == 0:
                            num_input += char
                            serport.write(char)
                        else:
                            pass
                    # Allow one decimal only
                    elif char == '.':
                        if '.' in num_input:
                            pass
                        else:
                            num_input += char
                            serport.write(char)
                    # Can only backspace if there is something to delete.
                    elif char in {'\b', '\x08', '\x7F'}:
                        if len(num_input) == 0:
                            pass
                        else:
                            num_input = num_input[0:-1]
                            serport.write(char)
                    # User can end input early by pressing s
                    elif char in {'s', 'S'}:
                        duty1.write(0)
                        state = S7_TEST_PRINT
                    # Pressing Enter inputs the number unless it is blank.
                    elif char in {'\r', '\n'}:
                        serport.write('\r\n')
                        if len(num_input) == 0:
                            pass
                        elif button_check == input_Kp:
                            Kp = float(num_input)
                            KpFlag.write(Kp)
                            num_input = ''
                            print('Choose a Ki Gain Value for motor 1')
                            button_check = input_Ki
                            
                        elif button_check == input_Ki:
                            Ki = float(num_input)
                            KiFlag.write(Ki)
                            num_input = ''
                            
                            if r_check:
                                button_check = input_y
                                state = S5_NUMBER 
                                print('Choose a Set Point Velocity for motor 1')
                            else:
                                button_check = 0
                                state = S1_CMD
                            
                        elif button_check == input_y:
                            ## y is the set point velocity 
                            y = float(num_input)
                            num_input = ''
                            button_check = 0
                            if r_check:
                                state = S9_CL_GATHER
                                print('Running step response')
                            else:
                                state = S1_CMD
                                yFlag.write(y)
                            
                        else:
                            duty = float(num_input)
                            if duty > 100:
                                duty = 100
                            elif duty < -100:
                                duty = -100
                                
                            if button_check == input_m:
                                state = S1_CMD
                                duty1.write(duty)
                                num_input = ''
                                button_check = 0
                                
                            elif button_check == input_M:
                                state = S1_CMD
                                duty2.write(duty)
                                serport.write('\r\n')
                                num_input = ''
                                button_check = 0
                                
                            elif button_check == input_t:
                                state = S8_FIND_VELOCITY
                                duty1.write(duty)
                                num_input = ''
                                velocity_index = 0
                                testArray.append(duty)
            
            elif state == S6_TEST:
                state = S5_NUMBER
                button_check = input_t
                print('Enter desired motor 1 duty cycle:')
            
            elif state == S7_TEST_PRINT:
                if array_index < test_index:
                    # print the comma separated values for plotting
                    print(f'{(testArray[array_index]):.2f}, {(test_velocityArray[array_index]):.2f}')
                    array_index +=1
                else:
                    state = S1_CMD
            
            elif state == S8_FIND_VELOCITY:
                # Wait for the motor to get up to speed, saving the velocity after 2 seconds
                if velocity_index < 200:
                    velocity_final = encoderData1.read()[3]
                    velocity_index += 1
                else:
                    state = S6_TEST
                    # print the entered duty and final velocity (after getting up to speed)
                    test_velocityArray.append(velocity_final)
                    test_index += 1
                    print(f' Duty {duty} %, Velocity {velocity_final} rad/s')
            
            elif state == S9_CL_GATHER:
                if current_size < 100:
                    yFlag.write(0)
                    timeArray[current_size], NA, NA, velocityArray[current_size], dutyArray[current_size] = encoderData1.read()
                    current_size += 1
                    
                elif 100 <= current_size <= 300: 
                    yFlag.write(y)
                    timeArray[current_size], NA, NA, velocityArray[current_size], dutyArray[current_size] = encoderData1.read()
                    current_size += 1
                    
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn == 's':
                            state = S10_CL_PRINT
                            
                    if current_size > 300:
                        state = S10_CL_PRINT
            
            elif state == S10_CL_PRINT:
                duty1.write(0)
                yFlag.write(0)
                if print_index < current_size:
                    print(f'{((timeArray[print_index]-timeArray[0])/1000):.2f}, {(velocityArray[print_index]):.2f}, {(dutyArray[print_index]):.2f}')
                    print_index += 1
                else:
                    state = S1_CMD
            
            next_time = ticks_add(next_time, period)
            yield state
            
        else:
            yield None