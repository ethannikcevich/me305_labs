'''!@file                   encoder.py
    @brief                  A driver for reading from Quadrature Encoders
    @details                Creates a class called Encoder that establishes
                            functions used for the encoder
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/23/22
'''
import pyb
from math import pi
import time 

class Encoder:
    '''!@brief              Interface with quadrature encoders
        @details            Creates a list of functions to be used
    '''
    def __init__ (self, timNum, period, pin1, pin2):
        '''!@brief          Sets up the encoder object
            @details        Creates and encoder object using channels on the Nucleo
            @param timNum   Decides which timer will be used
            @param period   The period of the timer
            @param pin1     First pin used by encoder
            @param pin2     Second pin used by encoder
        '''
        self.period = period
        self.tim = pyb.Timer(timNum, prescaler = 0, period = self.period)
        self.t4ch1 = self.tim.channel(1, pyb.Timer.ENC_AB, pin = pin1)
        self.t4ch2 = self.tim.channel(2, pyb.Timer.ENC_AB, pin = pin2)
        self.position = 0
        self.prev = self.tim.counter()
        self.prev_time = time.ticks_us()
        
    def zero(self):
        '''!@brief     Zeros the encoder value
            @details   Sets the position of the encoder to zero
        ''' 
        self.position = 0
        self.prev = self.tim.counter()
    
    def update(self):
        '''!@brief     Updates the encoder postition
            @details   To update the encoder's position, the change in position between updates is added to the position. To prevent overflow, delta must be corrected to account for the use of a 16 bit counter
        ''' 
        self.cur = self.tim.counter()
        self.cur_time = time.ticks_us()
        self.delta = (self.cur - self.prev) 
        self.tick_diff = time.ticks_diff(self.cur_time,self.prev_time)
        if self.delta > 32767:
            self.delta -= 65536
        elif self.delta < -32767:
            self.delta += 65536
        
        # convert ticks to radians
        self.delta = -self.delta*2*pi/4000
        
        # convert radians to rad/s
        self.omega = self.delta/self.tick_diff/1E-6
        
        # update the position
        self.position += self.delta 
        
        self.prev = self.cur
        self.prev_time = self.cur_time
        
        
    def get_position(self):
        '''!@brief             Returns encoder position in radians
            @details           Returns the position found in update() for use by other tasks
            @return position   position of the encoder
        ''' 
        return self.position
    
    def get_delta(self):
        '''!@brief          Returns encoder delta
            @details        Returns the delta found in update() for use by other tasks
            @return delta   delta of the encoder
        ''' 
        return self.delta

    def get_velocity(self):
        '''!@brief          Returns encoder angular velocity (rad/s)
            @details        Returns the velocity found in update() for use by other tasks
            @return omega   Encoder velocity
        '''
        return self.omega
    
    def get_tick_diff(self):
        '''! @brief              Returns the change in time between previous and current time
             @details            Returns the change in time between previous and current time, which was found in update()
             @return tick_diff   Change in time between previous and current time
        '''
        return self.tick_diff