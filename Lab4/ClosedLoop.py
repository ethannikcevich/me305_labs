'''!@file       ClosedLoop.py
    @brief      File for Closed Loop Control
    @details    Class that determines actuation based on gain values, reference speed, and measured speed
    @author     Ethan Nikcevich
    @author     Johnathan Dietz
    @date       2/21/22
'''

class ClosedLoop:
    '''!@brief     A class for closed loop control
        @details   Uses a PI controller to determine the actuation needed for closed loop control
    '''
    def __init__ (self, Kp, Ki, limit1, limit2):
        '''!@brief          Initializes a closed loop controller
            @details        Uses a PI controller, requiring Kp and Ki
            @param Kp       proportional gain
            @param Ki       integral gain
            @param limit1   high saturation limit (100)
            @param limit2   low saturation limit (-100)
        '''
        self.Kp = Kp
        self.Ki = Ki
        self.limit1 = limit1
        self.limit2 = limit2
        self.actuation = 0
        self.error_sum = 0
        self.Omega_ref = 0

    
    def run(self, Omega_meas, tick_diff):
        '''! @brief              Finds the actuation (duty) needed
             @details            Uses the equation for PI control
             @param Omega_meas   measured velocity of the motor
             @param tick_diff    difference in time between data collection from the motor
             @return actuation   The duty the motor should be set to for closed loop control
        '''
        
        self.error_sum += (self.Omega_ref - Omega_meas)*(tick_diff/1E6)
        
        self.actuation = self.Kp*((self.Omega_ref - Omega_meas) + self.Ki*self.error_sum)
        
        # if actuation value is too high, limit it to the max
        if self.actuation >= self.limit1:
            self.actuation = self.limit1
        # if actuation value is too low, limit it to the min
        elif self.actuation <= self.limit2:
            self.actuation = self.limit2
        
        return self.actuation
        
    def set_Gain(self, Kp, Ki):
        '''!@brief      Set gain values to user's input
            @details    This takes the user's desired gain values to be used in closed loop control
            @param Kp   proportional gain
            @param Ki   integral gain
        '''
        self.Kp = Kp
        self.Ki = Ki
        
    def set_Omega_ref(self, Omega_ref):
        '''!@brief             Set reference velocity of the motor
            @details           The reference velocity is used to determine actuation needed to reach it.
            @param Omega_ref   reference velocity
        '''
        self.Omega_ref = Omega_ref
    
    def get_Actuation(self):
        '''!@brief              returns the actuation value
            @details            The actuation value can be returned without calculating a new actuation value through this function
            @return actuation   The duty the motor should be set to for closed loop control
        '''
        return self.actuation