'''!@file                   task_encoder.py
    @brief                  Retrieves values from the encoder
    @details                Regularly retrieves the position, delta, and velocity of the encoder
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/19/22
'''
   
import time
import micropython

S0_UPDATE = micropython.const (0)
S1_ZERO = micropython.const(1)

def run(taskname, period, zFlag, encoderData, encoder, tick_diff, ClosedLoop):
    '''!@brief              Updates regularly and zeros the encoder
        @details            Retrieves and passes values to the encoder
        @param taskname     Name of the task, To run multiple tasks
        @param period       The period in which the task is run
        @param zFlag        Used to transistion to the zero state
        @param encoderData  Time, position, delta, and duty of the encoder.
        @param encoder      the encoder object being worked with (1 or 2)
        @param tick_diff    delta between start and current time
        @param ClosedLoop   Contains the gain and duty cycle
    '''
    
    # Starting state is S0
    state = S0_UPDATE

    start_time = time.ticks_us()
    next_time = time.ticks_add(start_time, period)
     
    while True:
        current_time = time.ticks_us()
        if time.ticks_diff(current_time, next_time) >= 0:
                
            # Always want to update the encoder's values
            if state == S0_UPDATE:
                encoder.update()
                encoderData.write((time.ticks_ms(), encoder.get_position(), encoder.get_delta(), encoder.get_velocity(), ClosedLoop.get_Actuation()))
                tick_diff.write(encoder.get_tick_diff())
                if zFlag.read():
                    state = S1_ZERO
            # If z was pressed, the encoder's position is zeroed.
            elif state == S1_ZERO:
                encoder.zero()
                zFlag.write(False)
                state = S0_UPDATE

            next_time = time.ticks_add(next_time, period)
            yield state
        else:
            yield None
