'''!@file                   main.py
    @brief                  Main file to initiate the program
    @details                Establishes flags for state transitions and calls 
                            the User and Encoder tasks
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/23/22
'''
import shares
import task_user
import task_encoder
import task_ClosedLoop
import task_driver
import task_enable
import DRV8847
import encoder
import ClosedLoop
import pyb

# State transition flags
zFlag = shares.Share(False)
eFlag = shares.Share(False)
cFlag = shares.Share(False)
kFlag = shares.Share(False)

period = 10_000

# Variables for changing the duty cycle of motors 1 and 2 respectively
duty1 = shares.Share(0)
duty2 = shares.Share(0)

# encoderData1 will contain the time, position, delta, and velocity data of the encoder
encoderData1 = shares.Share((0,0,0,0,0))

# Closed loop shares
KpFlag = shares.Share(0)
KiFlag = shares.Share(0)
yFlag = shares.Share(0)
wFlag = shares.Share(False)
tick_diff = shares.Share(0)

# Creating variables for the necessary pins
pinB0 = pyb.Pin(pyb.Pin.cpu.B0)
pinB1 = pyb.Pin(pyb.Pin.cpu.B1)
pinB2 = pyb.Pin(pyb.Pin.cpu.B2, mode = pyb.Pin.OUT_PP)
pinB4 = pyb.Pin(pyb.Pin.cpu.B4)
pinB5 = pyb.Pin(pyb.Pin.cpu.B5)
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
pinA15 = pyb.Pin(pyb.Pin.cpu.A15, mode = pyb.Pin.OUT_PP)

# Initiating an encoder object
encoder1 = encoder.Encoder(4, 65535, pinB6, pinB7)

# Initiating a motor driver object
driver = DRV8847.DRV8847(3, pinA15, pinB2)
motor1 = driver.motor(pinB4, pinB5, 1, 2)
motor2 = driver.motor(pinB0, pinB1, 3, 4)

# Initiating Closed Loop object
ClosedLoop = ClosedLoop.ClosedLoop(KpFlag, KiFlag, 100, -100)

# Want the motors enabled to start.
driver.enable()

# Runs on program startup:
if __name__ == '__main__':
    
    # Printing Help Command on startup
    task_user.help_command()
    
    # The list of tasks that will be iterated through, allowing for multitasking by the program
    taskList = [task_user.taskUser('Task User', period, zFlag, eFlag, cFlag, encoderData1, duty1, duty2, KpFlag, KiFlag, yFlag, wFlag),
                task_encoder.run('Task Encoder', period, zFlag, encoderData1, encoder1, tick_diff, ClosedLoop),
                task_driver.driver('Task Motor 1', period, motor1, duty1),
                task_driver.driver('Task Motor 2', period, motor2, duty2),
                task_enable.run('Task Safety', period, driver, eFlag, cFlag),
                task_ClosedLoop.run('Task Closed Loop', period, kFlag, ClosedLoop, encoderData1, wFlag, KpFlag, KiFlag, yFlag, duty1, tick_diff)]
    
    # Run the tasks on a loop
    while True:
        try:
            for task in taskList:
                next(task)
        except KeyboardInterrupt:
            driver.disable() 
            print('Program Terminating')
            break
    