'''!@file                   task_ClosedLoop.py
    @brief                  Sets the actuation (duty cycle) for the Closed Loop control.
    @details                This task takes the desired omega and measured omega, determines the needed actuation (duty), and writes it to the driver task.
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/19/22
'''

import time
import micropython

S0_INIT = micropython.const(0)
S1_RUN = micropython.const (1)

def run(taskname, period, kFlag, ClosedLoop, encoderData, wFlag, KpFlag, KiFlag, yFlag, duty, tick_diff):
    '''!@brief             Closed Loop Task
        @details           This function sets the gain values and updates the omega reference value, setting the duty cycle according to the closed loop.
        @param taskname    Name of the task, To run multiple tasks
        @param period      The period in which the task is run
        @param kFlag       Indicates if K has been pressed (gain input)
        @param ClosedLoop  Contains the gain and duty cycle
        @param encoderData Time, position, delta, and duty of the encoder.
        @param wFlag       Indicates if w has been pressed (closed loop toggle)
        @param KpFlag      Used to send the Kp value between tasks
        @param KiFlag      Used to send the Ki value between tasks
        @param yFlag       Used to send the reference velocity value between tasks
        @param duty        Used to send the duty value between tasks
        @param tick_diff   delta between start and current time
    '''

    # Starting state is S0
    state = S0_INIT

    start_time = time.ticks_us()
    next_time = time.ticks_add(start_time, period)
     
    while True:
        current_time = time.ticks_us()
        if time.ticks_diff(current_time, next_time) >= 0:
           
            if state == S0_INIT:
                state = S1_RUN
            
            elif state == S1_RUN:
                if wFlag.read():
                    ClosedLoop.set_Gain(KpFlag.read(), KiFlag.read())
                    ClosedLoop.set_Omega_ref(yFlag.read())
                    duty.write(ClosedLoop.run(encoderData.read()[3], tick_diff.read()))
                    
            next_time = time.ticks_add(next_time, period)
            yield state
        else:
            yield None