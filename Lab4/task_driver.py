'''!@file    task_driver.py
    @brief   Task used for controlling motors' duty cycle
    @details Uses provided information to control the speed of the motors
    @author  Ethan Nikcevich
    @author  Johnathan Dietz
    @date    2/23/22
'''

from time import ticks_us, ticks_diff, ticks_add

def driver(taskName, period, motor, duty):
    '''! @brief          An in-between task that calls the motor object's set_duty function
         @details        Takes a duty cycle and passes it to the motor object's set duty function
         @param taskName Name of the task, To run multiple tasks
         @param period   The period in which the task is run
         @param motor    The motor object being worked with (1 or 2)
         @param duty     Used to send the duty value between tasks
    '''
    
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
     
    while True:
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
            # Set the motor's duty to the read value.
            motor.set_duty(duty.read())
            
            next_time = ticks_add(next_time, period)
        else:
            yield None