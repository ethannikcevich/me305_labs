'''!@file                   task_enable.py
    @brief                  enables motors
    @details                enables motors when e is pressed and in startup.
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/19/22
'''
   
from time import ticks_us, ticks_diff, ticks_add
import micropython

S0_INIT = micropython.const(0)
S1_CMD = micropython.const(1)

def run(taskname, period, driver, eFlag, cFlag):
    '''!@brief           enables motors
        @details         Function that enables motors when e is pressed and in startup
        @param taskname  Name of the task, To run multiple tasks
        @param period    The period in which the task is run
        @param driver    object of the DRV8847 class
        @param cFlag     Indicates if c has been pressed
        @param eFlag     Indicates if e has been pressed
    '''
    
    # Starting state is S0
    state = S0_INIT
    
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    while True:
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                # enable the driver in startup
                driver.enable()
                state = S1_CMD
                
            elif state == S1_CMD:
                if eFlag.read():
                    # when e is pressed, eFlag is true from task_user, and driver is enabled
                    driver.enable()
                    eFlag.write(False)
                if cFlag.read():
                    # when c is pressed, cFlag is true from task_user, and faults are cleared here.
                    driver.enable()
                    cFlag.write(False)
                
            next_time = ticks_add(next_time, period)
            yield state
        else:
            yield None