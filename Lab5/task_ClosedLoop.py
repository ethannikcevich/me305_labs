'''!@file                   task_ClosedLoop.py
    @brief                  Sets the actuation (duty cycle) for the Closed Loop control.
    @details                This task takes the desired omega and measured omega, determines the needed actuation (duty), and writes it to the driver task.
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   3/17/22
'''
   
import time
import micropython


S0_INIT = micropython.const(0)
S1_OUTER_LOOP = micropython.const(1)
S2_INNER_LOOP = micropython.const(2)

def run(taskname, period, outer_loop, inner_loop, euler, eFlag, KpFlag, KdFlag, KiFlag, yFlag, duty, gyro, xyEuler, xyGyro, xyPosition, xyVelocity, dFlag, DFlag, KpFlag_outer, KdFlag_outer, KiFlag_outer, filter_data, angle):
    '''!@brief              Closed Loop Task
        @details            This function sets the gain values and updates the omega reference value, setting the duty cycle according to the closed loop.
        @param taskname     Name of the task, To run multiple tasks
        @param period       The period of the task
        @param kFlag        Indicates if K has been pressed (gain input)
        @param outer_loop   Contains the gain and duty cycle of the outer loop
        @param inner_loop   Contains the gain and duty cycle of the inner loop
        @param euler        Euler Angles
        @param eFlag        Indicates if e has been pressed (closed loop toggle)
        @param KpFlag       Used to send the Kp value of the inner loop between tasks
        @param KdFlag       Used to send the Kd value of the inner loop between tasks
        @param KiFlag       Used to send the Ki value of the inner loop between tasks
        @param yFlag        Used to send the reference velocity value between tasks
        @param duty         Used to send the duty value between tasks
        @param gyro         Angular velocities about X, Y, Z 
        @param xyEuler      x vs y axis for Euler angles
        @param xyGyro       x vs y axis for Gyro angular velocities
        @param xyPosition   x vs y axis for ball position
        @param xyVelocity   x vs y axis for ball velocity
        @param dFlag        Used to print the duty cycle of motor 1
        @param DFlag        Used to print the duty cycle of motor 2
        @param KpFlag_outer Used to send the Kp value of the outer loop between tasks
        @param KdFlag_outer Used to send the Kd value of the outer loop between tasks
        @param KiFlag_outer Used to send the Ki value of the outer loop between tasks
        @param filter_data  For filtering using alpha and  beta.
        @param angle        For carrying over angle from outer to inner loop
    '''
    
    # Starting state is S0
    state = S0_INIT
    start_time = time.ticks_us()
    next_time = time.ticks_add(start_time, period)
    
    while True:
        current_time = time.ticks_us()
        if time.ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                state = S1_OUTER_LOOP
            elif state == S1_OUTER_LOOP:
                if eFlag.read():
                    outer_loop.set_Gain(KpFlag_outer.read(), KdFlag_outer.read(), KiFlag_outer.read())
                    outer_loop_actuation = outer_loop.run(filter_data.read()[xyPosition], filter_data.read()[xyVelocity], 0, 0, filter_data.read()[4])
                    angle.write(outer_loop_actuation)
                    state = S2_INNER_LOOP
                    
            elif state == S2_INNER_LOOP:
                inner_loop.set_Gain(KpFlag.read(), KdFlag.read(), KiFlag.read())
                inner_loop_actuation = inner_loop.run(euler.read()[xyEuler], gyro.read()[xyGyro], angle.read(), 1, 1)
                duty.write(inner_loop_actuation)
                if dFlag.read():
                    if eFlag.read():
                        if taskname == 'Task Closed Loop 1':
                            dFlag.write(False)
                            print(f'{taskname}: {inner_loop_actuation}')
                if DFlag.read():
                    if eFlag.read():
                        if taskname == 'Task Closed Loop 2':
                            DFlag.write(False)
                            print(f'{taskname}: {inner_loop_actuation}')
                state = S1_OUTER_LOOP
            next_time = time.ticks_add(next_time, period)
            yield state
        else:
            yield None