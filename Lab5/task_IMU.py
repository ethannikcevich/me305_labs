'''!@file    task_IMU.py
    @brief   File for all IMU interactions (retrieving measurement data and calibrating)
    @details Task used for IMU interactions, such as calibration and retrieving calibration data
    @author  Ethan Nikcevich
    @author  Johnathan Dietz
    @date    3/17/22
'''
   
import time
import micropython
import gc
import os

S0_INIT = micropython.const(0)
S1_RUN = micropython.const (1)
S2_CALIBRATE = micropython.const (2)
S3_SET_CALIBRATION_VALUES = micropython.const (3)
S4_END_CALIBRATION = micropython.const (4)

def run(taskname, period, calibration_data, BNO055, euler, gyro):
    '''!@brief                   Runs IMU task
        @details                 Runs IMU task with passed in task period, calibration data, the IMU driver object, and IMU data.
        @param taskname          To run multiple tasks
        @param period            period of how frequently the task is run
        @param calibration_data  combined magnetometer, accelerometer, gyroscope, and system data
        @param BNO055            BNO055 IMU object
        @param euler             Euler Angles
        @param gyro              Angular velocities about X, Y, Z
    '''
    
    state = S0_INIT
    index = 0
    start_time = time.ticks_us()
    next_time = time.ticks_add(start_time, period)
    
    # For calibrating the board and reading from a calibrated txt file
    calibration_txt = 'IMU_cal_coeffs.txt'
    calibration_str = ''
     
    while True:
    
        current_time = time.ticks_us()
        if time.ticks_diff(current_time, next_time) >= 0:
            
            if state == S0_INIT:
                # check for calibration txt file
                if calibration_txt in os.listdir():
                    # if there is a calibration txt file, go to S4
                    state = S4_END_CALIBRATION
                else:    
                    # if there is not a calibration txt file, go to S4
                    state = S2_CALIBRATE
                    print('The IMU must be calibrated')
                    # change IMU to proper mode for calibration
                    BNO055.change_mode(12)
                    
            elif state == S1_RUN:                
                euler.write(BNO055.get_euler_angles())
                head, pitch, roll = euler.read()
                gyro.write(BNO055.get_omega())
                gyr_x, gyr_y, gyr_z = gyro.read()
                    
            elif state == S2_CALIBRATE:
                calibration_data.write(BNO055.get_calibration_status())
                mag, acc, gyr, sys = calibration_data.read()
                print('mag: ' + str(mag), 'acc: ' + str(acc), 'gyr: ' + str(gyr), 
                      'sys: ' +str(sys))
                if (mag, acc, gyr, sys) == (3, 3, 3, 3):  
                    state = S3_SET_CALIBRATION_VALUES
                    
            elif state == S3_SET_CALIBRATION_VALUES:    
                with open(calibration_txt, 'w') as f:
                    # Perform manual calibration
                    calibration_buffer = bytearray(22*[0])
                    gc.collect()
                    cal_co_barr = BNO055.get_calibration_coefficients(calibration_buffer)
                    print(cal_co_barr)
                    
                    for byte in cal_co_barr:
                        calibration_str += hex(byte)
                        calibration_str += ','
                        index += 1
                    if index == 22:
                        state = S1_RUN
                        calibration_str = calibration_str[:-1]
                        print(calibration_str)
                        f.write(calibration_str)
                        
            elif state == S4_END_CALIBRATION:
                    
                with open(calibration_txt, 'r') as f:
                    cal_data_string = f.readline()
                    set_calibration_data = bytearray([int(cal_value) for cal_value in cal_data_string.strip().split(',')])
                    print(set_calibration_data)
                    BNO055.change_mode(0)
                    BNO055.set_calibration_coefficients(set_calibration_data)
                state = S1_RUN
                BNO055.change_mode(12)
                    
            else:
                pass
            next_time = time.ticks_add(next_time, period)
            yield state
        else:
            yield None