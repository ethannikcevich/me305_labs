'''!@file                   main.py
    @brief                  Main file to initiate the program
    @details                Establishes flags for state transitions and calls 
                            the User and Encoder tasks
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   3/17/22
'''

import shares
import task_user
import task_ClosedLoop
import task_driver
import task_IMU
import motor
import BNO055
import ClosedLoop
import touchpad
import task_touchpad
from pyb import Pin

# inner loop gain values
KpFlag = shares.Share(0)
KdFlag = shares.Share(0)
KiFlag = shares.Share(0)

# outer loop gain values
KpFlag_outer = shares.Share(0)
KdFlag_outer = shares.Share(0)
KiFlag_outer = shares.Share(0)

angle1 = shares.Share(0)
angle2 = shares.Share(0)

yFlag = shares.Share(False)
eFlag = shares.Share(False)
dFlag = shares.Share(False)
DFlag = shares.Share(False)
calibration_check = shares.Share(False)

filter_data = shares.Share((0,0,0,0,0))

period = 10_000

# Variables for changing the duty cycle of motors 1 and 2 respectively
duty1 = shares.Share(0)
duty2 = shares.Share(0)

# encoderData1 will contain the time, position, delta, and velocity data of the encoder
encoderData1 = shares.Share((0,0,0,0,0))


calibration_data = shares.Share((0,0,0,0))
euler = shares.Share((0,0,0))
gyro = shares.Share((0,0,0))

# for touchpad
coord = shares.Share((0,0,0))

pinB0 = Pin(Pin.cpu.B0)
pinB1 = Pin(Pin.cpu.B1)
pinB4 = Pin(Pin.cpu.B4)
pinB5 = Pin(Pin.cpu.B5)

# Initiating motors
motor1 = motor.Motor(3, pinB4, pinB5, 1, 2)
motor2 = motor.Motor(3, pinB0, pinB1, 3, 4)

# Initiating Closed Loop objects, lowered saturation values for platform control (don't need full speed motors)
outer_loop1 = ClosedLoop.ClosedLoop(0, 0, 0, 12, -12)
outer_loop2 = ClosedLoop.ClosedLoop(0, 0, 0, 12, -12)
inner_loop1 = ClosedLoop.ClosedLoop(0, 0, 0, 45, -45)
inner_loop2 = ClosedLoop.ClosedLoop(0, 0, 0, 45, -45)

BNO055 = BNO055.BNO055()

Pad = touchpad.Touchpad(Pin.cpu.A0, Pin.cpu.A1, Pin.cpu.A6, Pin.cpu.A7, 176, 100)

if __name__ == '__main__':
    
    # Printing Help Command on startup
    task_user.help_command()
    
    # The list of tasks that will be iterated through, allowing for multitasking by the program
    taskList = [task_user.taskUser('Task User', 50_000, euler, gyro, KpFlag, KdFlag, KiFlag, yFlag, eFlag, dFlag, DFlag, KpFlag_outer, KdFlag_outer, KiFlag_outer, filter_data),
                task_driver.driver ('Task Motor 1', period, motor1, duty1),
                task_driver.driver ('Task Motor 2', period, motor2, duty2),
                task_ClosedLoop.run('Task Closed Loop 1', period, outer_loop1, inner_loop1, euler, eFlag, KpFlag, KdFlag, KiFlag, yFlag, duty1, gyro, 1, 1, 0, 1, dFlag, DFlag, KpFlag_outer, KdFlag_outer, KiFlag_outer, filter_data, angle1),
                task_ClosedLoop.run('Task Closed Loop 2', period, outer_loop2, inner_loop2, euler, eFlag, KpFlag, KdFlag, KiFlag, yFlag, duty2, gyro, 2, 0, 2, 3, dFlag, DFlag, KpFlag_outer, KdFlag_outer, KiFlag_outer, filter_data, angle2),
                task_IMU.run('Task IMU', period, calibration_data, BNO055, euler, gyro),
                task_touchpad.run('Task Touchpad', period, coord, Pad, filter_data, 0.85, 0.005, calibration_check)]
    
    # Run the tasks on a loop
    while True:
        try:
            for task in taskList:
                next(task)
        except KeyboardInterrupt:
            #reset all share values on interrupt
            KpFlag.write(0)
            KdFlag.write(0)
            KiFlag.write(0)
            duty1.write(0)
            duty2.write(0)
            print('Program Terminating')
            break