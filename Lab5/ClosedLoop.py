'''!@file       ClosedLoop.py
    @brief      File for Closed Loop Control
    @details    Class that determines actuation based on gain values, reference speed, and measured speed
    @author     Ethan Nikcevich
    @author     Johnathan Dietz
    @date       3/17/22
'''

class ClosedLoop:
    '''!@brief     A class for closed loop control
        @details   Uses a PID controller to determine the actuation needed for closed loop control
    '''
    
    def __init__ (self, Kp, Kd, Ki, limit1, limit2):
        '''!@brief          Initializes a closed loop controller
            @details        Uses a PID controller, requiring Kp, Ki, & Kd
            @param Kp       proportional gain
            @param Kd       derivative gain
            @param Ki       integral gain
            @param limit1   high saturation limit
            @param limit2   low saturation limit
        '''
        ## @brief   Proportional gain
        #  @details Set to the desired proportional gain for the loop object, inner or outer.
        self.Kp = Kp
        ## @brief   Derivative gain
        #  @details Set to the desired derivative gain for the loop object, inner or outer.
        self.Kd = Kd
        ## @brief   Integral gain
        #  @details Set to the desired integral gain for the loop object, inner or outer.
        self.Ki = Ki
        ## @brief   High saturation limit
        #  @details The highest value that the loop is allowed to produce.
        self.limit1 = limit1
        ## @brief   Low saturation limit
        #  @details The lowest value that the loop is allowed to produce.
        self.limit2 = limit2
        ## @brief   The duty value of the motor
        #  @details The duty the motor should be set to for closed loop control
        self.actuation = 0
        ## @brief   Reference value
        #  @details The value that the loop should use as a reference for determining error
        self.ref = 0
        ## @brief   Summation of position values (pos).
        #  @details Each time the loop is run, the input pos is added to pos_sum. This value is used in determining actuation.
        self.pos_sum = 0
        ## @brief   positive anti windup value
        #  @details to be combined with negative anti windup value to compute anti windup correction
        self.windup_p = 0
        ## @brief   negative anti windup value
        #  @details to be combined with negative anti windup value to compute anti windup correction
        self.windup_n = 0
        ## @brief   Ti = Kp/Ki
        #  @details Kp is proportional gain and Ki is integral gain
        self.Ti = 0
        ## @brief   Ts*Ti
        #  @details Ts is controller period [sec] and Ti is Kp/Ki
        self.TiTs = 0
        
    def run(self, pos, vel, ref, check, Flag):
        '''! @brief              Finds the actuation (duty) needed for closed loop control
             @details            Uses the equation for PID control
             @param pos          Angular position
             @param vel          Angular velocity
             @param ref          Desired reference angle
             @param check        boolean for closed loop control
             @param Flag         Second boolean for loop control
             @return             Returns the actuation (duty) needed for closed loop control
        '''
        ## @brief   Angular position value
        #  @details angular position of platform
        self.pos = pos
        self.pos_sum += self.pos
        ## @brief   Angular velocity value
        #  @details Angular velocity of platform
        self.vel = vel
        self.ref = ref
        if self.Ki != 0:
            self.Ti = self.Kp/self.Ki
        elif self.Ki == 0:
            self.Ti = 0
        if self.Ti != 0:
            self.TsTi = 10/self.Ti
        elif self.Ti == 0:
            self.TsTi = 0
        self.actuation = self.Kp*(self.ref - self.pos) - self.Kd*self.vel + ((self.TsTi*self.pos_sum) - (self.windup_p - self.windup_n))
        if check == 0:
            self.windup_p = self.actuation
        elif check == 1:
            self.actuation += 5
        if self.actuation > self.limit1:
            self.actuation = self.limit1
        elif self.actuation < self.limit2:
            self.actuation = self.limit2
        return -self.actuation
        if check == 0:
            if Flag == 0:
                self.actuation = 0
            self.windup_n = self.actuation
        return -self.actuation
        
    def set_Gain (self, Kp, Kd, Ki):
        '''!@brief      Set gain values to user's input
            @details    This takes the user's desired gain values to be used in closed loop control
            @param Kp   proportional gain
            @param Kd   derivative gain
            @param Ki   integral gain
        '''
        self.Kp = Kp
        self.Kd = Kd
        self.Ki = Ki
        
    def set_ref (self, ref):
        '''!@brief             Set reference velocity of the motor
            @details           The reference velocity is used to determine actuation needed to reach it.
            @param ref         reference velocity
        '''
        self.ref = ref
    
    def get_Actuation (self):
        '''!@brief              returns the actuation value
            @details            The actuation value can be returned without calculating a new actuation value through this function
            @return actuation   The duty the motor should be set to for closed loop control
        '''
        return self.actuation