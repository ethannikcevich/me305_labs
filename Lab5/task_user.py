'''!@file                   task_user.py
    @brief                  File for all user interactions
    @details                Interacts with the user by responding to keyboard 
                            inputs
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   3/17/22
'''

from time import ticks_us, ticks_add, ticks_diff, ticks_ms
import pyb
import array
import micropython
import gc

input_y = micropython.const(0)
input_Y = micropython(1)
input_Kp = micropython.const(2)
input_Kd = micropython.const(3)
input_Ki = micropython.const(4)
input_Kp_outer = micropython.const(5)
input_Kd_outer = micropython.const(6)
input_Ki_outer = micropython.const(7)

# States used inside task_user
S0_INIT = micropython.const(8)
S1_CMD = micropython.const (9)
S2_GATHER = micropython.const(10)
S3_GATHER_PRINT = micropython.const(11)
S4_NUMBER = micropython.const(12)

def help_command():
    '''!@brief    Command List
        @details  Prints a list of key inputs available to the user
    ''' 
    
    print('+---------------------------------------------------+')
    print('| Use the following commands:                       |')
    #print('| z or Z : Zero the position of encoder 1           |')
    #print('| p or P : Print the position of encoder 1          |')
    print('| d      : Print the duty cycle of motor 1          |')
    print('| D      : Print the duty cycle of motor 2          |')
    print('| p or P : Print the Euler angles                   |')
    print('| v or V : Print the angular velocities             |')
  #  print('| m : Enter a duty cycle for motor 1                |')
 #   print('| M : Enter a duty cycle for motor 2                |')
  #  print('| c or C : Clear a fault condition                  |')
    print('| g or G : Gather platform data                     |')
    print('| s or S : End processes prematurely                |')
    print('| e or E : Toggle closed-loop control               |')
    print('| k      : Enter gain values for the inner loop     |')
    print('| K      : Enter gain values for the outer loop     |')
    print('| h or H : Print this help message                  |')
    print('| Ctrl-C : Terminate Program                        |')
    print('+---------------------------------------------------+')  

def taskUser(taskname, period, euler, gyro, KpFlag, KdFlag, KiFlag, yFlag, eFlag, dFlag, DFlag, KpFlag_outer, KdFlag_outer, KiFlag_outer, filter_data):
    
    '''!@brief                  Main method that controls user interface.
        @details                Interprets all user commands
        @param taskname         To run multiple tasks
        @param period           period of how frequently the task is run
        @param zFlag            Used to transistion to the zero state
        @param euler            Euler Angles
        @param gyro             Angular velocities about X, Y, Z
        @param KpFlag           Used to send the Kp inner loop value between tasks
        @param KdFlag           Used to send the Kd inner loop value between tasks
        @param KiFlag           Used to send the Ki inner loop value between tasks
        @param yFlag            Used to send the reference velocity value between tasks
        @param eFlag            closed loop toggle transition
        @param dFlag            Used to print the duty cycle of motor 1
        @param DFlag            Used to print the duty cycle of motor 2
        @param KpFlag_outer     Used to send the Kp outer loop value between tasks
        @param KdFlag_outer     Used to send the Kd outer loop value between tasks
        @param KiFlag_outer     Used to send the Ki outer loop value between tasks
        @param filter_data      For filtering using alpha and  beta.
    '''

    # Starting state is S0
    state = S0_INIT
    
    # To create an internal timer
    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    # Arrays used for collecting and printing of data
    timeArray = array.array('f', 301*[0])
    gc.collect()
    x_position_Array = array.array('f', timeArray)
    x_velocity_Array = array.array('f', timeArray)
    gc.collect()
    y_position_Array = array.array('f', timeArray)
    y_velocity_Array = array.array('f', timeArray)
    gc.collect()
    
    index = 0
    # The current number of data points during collection
    current_size = 0
    # Index used inside print of collected data
    print_index = 0 
    # For user input of numbers
    num_input = ''
    serport = pyb.USB_VCP()
    print_str = ''
    
    while True:
        current_time = ticks_us()
        if ticks_diff(current_time, next_time) >= 0:
 
            if state == S0_INIT:
                state = S1_CMD
                
            # State 1 waits for user input to select an action
            elif state == S1_CMD:
                if serport.any():
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'h', 'H'}:
                        help_command()               
                    
                    elif charIn in {'p', 'P'}:
                        z, y, x = euler.read()
                        print(f'Heading: {z:.2f} [deg], Roll: {y:.2f} [deg], Pitch: {x:.2f} [deg]')
                                        
                    elif charIn == 'd':
                        dFlag.write(True)
                        
                    elif charIn == 'D':
                        DFlag.write(True)
                        
                    elif charIn in {'v', 'V'}:
                        xGyro, yGyro, zGyro = gyro.read()
                        print(f'Angular velocities about X, Y, and Z: {xGyro:.2f} [rad/s], {yGyro:.2f} [rad/s], {zGyro:.2f} [rad/s]')
                    
                    elif charIn in {'g', 'G'}:
                        state = S2_GATHER
                        print_index = 0
                        current_size = 0    
                        print('Collecting')
                        
                    elif charIn in {'e', 'E'}:
                        if eFlag.read():
                            print('Disabled Closed Loop control')
                            eFlag.write(False)
                        else:
                            print('Enabled Closed Loop control')
                            eFlag.write(True)
                        
                    elif charIn == 'k':
                        state = S4_NUMBER
                        button_check = input_Kp
                        print('Choose a Kp Gain Value for the inner control loop')
                        
                    elif charIn == 'K':
                        state = S4_NUMBER
                        button_check = input_Kp_outer
                        print('Choose a Kp Gain Value for the outer control loop')
                    
            elif state == S2_GATHER:
                if current_size < 301:
                    timeArray[current_size] = ticks_ms()
                    x_position_Array[current_size], x_velocity_Array[current_size], y_position_Array[current_size], y_velocity_Array[current_size]
                    current_size += 1
                    if serport.any():
                        charIn = serport.read(1).decode()
                        if charIn == 's':
                            state = S3_GATHER_PRINT
                            print('Data Collection Stopped')
                    if current_size > 300:
                        state = S3_GATHER_PRINT
                        
            # This state is for printing the data gathered
            elif state == S3_GATHER_PRINT:
                with open('Gather.csv', 'a+') as f:
                    if print_index < current_size:
                        gather_values = [round((timeArray[print_index]-timeArray[0])/1000, 2), 
                                round(x_position_Array[print_index], 2), 
                                round(x_velocity_Array[print_index], 2), 
                                round(y_position_Array[print_index], 2), 
                                round(y_velocity_Array[print_index], 2)]
                        for x in gather_values:
                            index += 1
                            print_str += str(x)
                            print_str += ','
                        if index == 5:
                            index = 0
                            f.write(print_str)
                            f.write('\n')
                            print(print_str)
                            print_index += 1
                            print_str = ''
                    elif print_index == current_size:
                        state = S1_CMD
            
            # This state is for user inputs of numbers, used by assigning duty cycles and closed loop operations
            elif state == S4_NUMBER:
                if serport.any():
                    char = serport.read(1).decode()
                    if char.isdigit():
                        num_input += char
                        serport.write(char)
                    # allow - in first spot to create negative numbers
                    elif char == '-':
                        if len(num_input) == 0:
                            num_input += char
                            serport.write(char)
                    # Allow one decimal only
                    elif char == '.':
                        if '.' in num_input:
                            pass
                        else:
                            num_input += char
                            serport.write(char)
                    # Can only backspace if there is something to delete.
                    elif char in {'\b', '\x08', '\x7F'}:
                        if len(num_input) > 0:
                            num_input = num_input[0: -1]
                            serport.write(char)
                    # Pressing Enter inputs the number unless it is blank.
                    elif char in {'\r', '\n'}:
                        serport.write('\r\n')
                        if len(num_input) == 0:
                            pass
                        elif button_check == input_Kp:
                            Kp_inner = float(num_input)
                            KpFlag.write(Kp_inner)
                            num_input = ''
                            print('Choose a Kd Gain Value for the inner control loop')
                            button_check = input_Kd
                        elif button_check == input_Kd:
                            Kd_inner = float(num_input)
                            KdFlag.write(Kd_inner)
                            num_input = ''
                            print('Choose a Ki Gain Value for the inner control loop')
                            button_check = input_Ki
                        elif button_check == input_Ki:
                            Ki_inner = float(num_input)
                            KiFlag.write(Ki_inner)
                            num_input = ''
                            button_check = 0
                            state = S1_CMD
                        elif button_check == input_Kp_outer:
                            Kp_outer = float(num_input)
                            KpFlag_outer.write(Kp_outer)
                            num_input = ''
                            button_check = input_Kd_outer
                            print('Choose a Kd Gain Value for the outer control loop')
                        elif button_check == input_Kd_outer:
                            Kd_outer = float(num_input)
                            KdFlag_outer.write(Kd_outer)
                            num_input = ''
                            button_check = input_Ki_outer
                            print('Choose a Ki Gain Value for the outer control loop')
                        elif button_check == input_Ki_outer:
                            Ki_outer = float(num_input)
                            KiFlag_outer.write(Ki_outer)
                            num_input = ''
                            button_check = 0
                            state = S1_CMD
                    
            next_time = ticks_add(next_time, period)
            yield state
        else:
            yield None

