'''!@file                   task_encoder.py
    @brief                  File for all encoder interactions
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/2/22
'''

import encoder
import pyb
from time import ticks_diff, ticks_add, ticks_us, ticks_ms



def taskEncoder(taskname, period, zFlag, pFlag, dFlag, gFlag, position, delta, Pos, Time):
    '''!@brief             Encoder Task
        @details           Interacts with the encoder class to zero the 
                           encoder, retrieve the position or delta, and 
                           gather data for 30 seconds
        @param taskName    The name of the task is a string
        @param period      The period of the task in microseconds as an integer
        @param zFlag       Used to transistion to the zero state
        @param pFlag       Used to transistion to the print (position) state
        @param dFlag       Used to transistion to the (print) delta state
        @param gFlag       Used to transistion to the gather state
        @param position    Used to retrieve the position from the encoder
        @param delta       Used to retrieve the delta from the encoder
        @param Pos         Used for the gather function, position array
        @param Time        Used for the gather function, time array
    '''
    start_time = ticks_us()
    next_Time = ticks_add(start_time, period)
    encoder_1 = encoder.Encoder(pyb.Pin.board.PB6, pyb.Pin.board.PB7, 4)  
    
    
    while True:
        current_Time = ticks_us()
        encoder_1.update()
        if ticks_diff(current_Time,next_Time) >= 0:
            next_Time = ticks_add(next_Time,period)
                       
            if zFlag.read():
                # Zero the encoder
                encoder_1.zero()          
                zFlag.write(False)
            
            elif pFlag.read():
                # Send the encoder position to task_user
                position.write(encoder_1.get_position())
                pFlag.write(False)
            
            elif dFlag.read():
                # Send the encoder delta to task_user
                delta.write(encoder_1.get_delta())
                dFlag.write(False)
            
            elif gFlag.read():
                encoder_1.update()
                # Send the encoder position and time collected in ms to task_user
                Time.write(ticks_ms())
                Pos.write(encoder_1.get_position())             
                 
            yield None
            
        else:
            yield None
    

