'''!@file                   encoder.py
    @brief                  A driver for reading from Quadrature Encoders
    @details                Creates a class called Encoder that establishes
                            functions used for the encoder
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/2/22
'''


import pyb



class Encoder:
    '''!@brief              Interface with quadrature encoders
        @details            Creates a list of functions to be used
    '''
    
    def __init__(self, chA_pin, chB_pin,TimNum):
        '''!@brief          Constructs an encoder object
            @details        Constructor block to define input parameters
            @param chA_pin  First pin in the timer
            @param chB_pin  Second pin in the timer
            @param TimerNum 
            @return Encoder object with EncoderName, Text relaying an encoder has 
                    been created
        '''
        
        # creates timer that tracks position
        self.tim = pyb.Timer(TimNum, prescaler=0, period = 65535)
        self.ch1 = self.tim.channel(1, pyb.Timer.ENC_AB,pin=chA_pin)
        self.ch2 = self.tim.channel(2, pyb.Timer.ENC_AB,pin=chB_pin)
        # used to determine delta
        self.prev = 0
        self.cur = 0
        # encoder's position
        self.position = 0
    
    def zero(self):
        '''!@brief          Resets the encoder position to zero
            @return Text relaying that the current location is zero
        '''
        self.position = 0
             
    
    def update(self):
        '''!@brief          Updates encoder position and delta
            @details        Updates current position and delta while correcting
                            for overshoots and undershoots
        '''
        
        self.delta = 0
        AR = 65535 # Auto-reload value
        self.cur = self.tim.counter()
        self.delta = self.cur - self.prev
                
        if self.delta > (AR + 1)/2:
            self.delta -= AR + 1
            self.position += self.delta
        elif self.delta < (-AR + 1)/2:
            self.delta += AR + 1 
            self.position += self.delta
        else:
            self.position += self.delta
        self.prev = self.cur
                 
        
    def get_position(self):
        '''!@brief          Returns encoder position
            @details        Returns encoder position to taskUser
            @return         The position of the encoder shaft
        '''
        return self.position

        
    
    def get_delta(self):
        '''!@brief          Returns encoder delta
            @details        Sends delta when called
            @return         The change in position of the encoder shaft
                            between the two most recent updates
        '''
        return self.delta