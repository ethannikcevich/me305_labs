'''!@file                   main.py
    @brief                  Main file to initiate the program
    @details                Establishes flags for state transitions and calls 
                            the User and Encoder tasks
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/2/22
'''
import shares
from task_encoder import taskEncoder
from task_user import taskUser


zFlag = shares.Share(False)
pFlag = shares.Share(False)
dFlag = shares.Share(False)
gFlag = shares.Share(False)
sFlag = shares.Share(False)
position = shares.Share(0)
delta = shares.Share(0)
Pos = shares.Share(0)
Time = shares.Share(0)


task1 = taskUser('taskUser', 10_000, zFlag, pFlag, dFlag, gFlag, position, delta, Pos, Time)
task2 = taskEncoder('taskEncoder',10_000, zFlag, pFlag, dFlag, gFlag, position, delta, Pos, Time)

# create a task list to iterate through
taskList = [task1,task2]


while True:
    try:
        for task in taskList:
            next(task)
    
    except KeyboardInterrupt:
        break
        
print('Program Terminating')

