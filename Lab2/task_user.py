'''!@file                   task_user.py
    @brief                  File for all user interactions
    @details                Interacts with the user by responding to keyboard 
                            inputs, printing a help statement or calling the 
                            encoder task to retrieve requested encoder data
    @author                 Ethan Nikcevich
    @author                 Johnathan Dietz
    @date                   2/2/22
'''

from time import ticks_diff, ticks_add, ticks_us, ticks_ms
from pyb import USB_VCP
import micropython
import array as arr


S0_INIT = micropython.const(0)
S1_CMD  = micropython.const(1)
S2_ZERO = micropython.const(2)
S3_POSITION  = micropython.const(3)
S4_DELTA  = micropython.const(4)
S5_GATHER = micropython.const(5)
S6_HELP  = micropython.const(6)


def taskUser(taskname, period, zFlag, pFlag, dFlag, gFlag, position, delta, Pos, Time):
    '''!@brief             User Interface Task
        @details           Provides a user interface over a VCP and to interact 
                           with a user through a Serial Monitor/Termianal such as Putty
        @param taskName    The name of the task is a string
        @param period      The period of the task in microseconds as an integer
        @param zFlag       Used to transistion to the zero state
        @param pFlag       Used to transistion to the print (position) state
        @param dFlag       Used to transistion to the (print) delta state
        @param gFlag       Used to transistion to the gather state
        @param position    Used to retrieve the position from the encoder
        @param delta       Used to retrieve the delta from the encoder
        @param Pos         Used for the gather function, position array
        @param Time        Used for the gather function, time array
    '''
    state = S0_INIT
    print("+-----------------------------------------------------+")
    print("| Use the following Commands:                         |")
    print("| z or Z   Zero the position of encoder 1             |")
    print("| p or P   Print the position of encoder 1            |")
    print("| d or D   Print the delta (speed) of encoder 1       |")
    print("| g or G   Collect data from encoder 1 for 30 seconds |")
    print("| s or S   End data collection early                  |")
    print("| h or H   Print this help message                    |")
    print("| Ctrl-C   Terminate Program                          |")
    print("+-----------------------------------------------------+")
    

    start_time = ticks_us()
    next_time = ticks_add(start_time, period)
    
    # Used for interacting with the keyboard
    ser = USB_VCP()
    
    # The finite state machine must run indefinitely.
    while True:
        current_time = ticks_us()
        if ticks_diff(current_time,next_time) >= 0:
            next_time = ticks_add(next_time, period)
            
            
            if state == S0_INIT:         # Start-up State     
                # State Transition
                state = S1_CMD 
                
            # State 1 code
            elif state == S1_CMD:        # Update State
                
                if ser.any():
                    # turn user key press into a string
                    charIn = ser.read(1).decode()
                    
                    if charIn in {'z','Z'}:
                        print("Zeroing Encoder")
                        zFlag.write(True)
                        # State Transition
                        state = S2_ZERO
                    elif charIn in {'p', 'P'}:
                        print("Encoder Position")
                        pFlag.write(True)
                        # State Transition
                        state = S3_POSITION
                    elif charIn in {'d', 'D'}:
                        print("Delta Position")
                        dFlag.write(True)
                        # State Transition
                        state = S4_DELTA
                    elif charIn in {'g', 'G'}:
                        print("Collecting Data")
                        gFlag.write(True)
                        # Create arrays to be filled with position and time data
                        positionArray = arr.array('l', 3001*[0])
                        timeArray = arr.array('l', 3001*[0])
                        # The time that the data collection begins is recorded to stop after 30 seconds
                        start = ticks_ms()
                        # State Transition
                        state = S5_GATHER
                        # create index
                        index = 0
                    elif charIn in {'h', 'H'}:
                        print("+-----------------------------------------------------+")
                        print("| Use the following Commands:                         |")
                        print("| z or Z   Zero the position of encoder 1             |")
                        print("| p or P   Print the position of encoder 1            |")
                        print("| d or D   Print the delta (speed) of encoder 1       |")
                        print("| g or G   Collect data from encoder 1 for 30 seconds |")
                        print("| s or S   End data collection early                  |")
                        print("| h or H   Print this help message                    |")
                        print("| Ctrl-C   Terminate Program                          |")
                        print("+-----------------------------------------------------+")
                    else:
                        # In case the user types an unexpected key
                        print(f"Invalid Input of {charIn} at t={ticks_diff(current_time,start_time)/1e6}[s].")
                    
            elif state == S2_ZERO:           # Zero Encoder State
                if not zFlag.read():
                    print("Encoder Zeroed")
                    state = S1_CMD
                    
            elif state == S3_POSITION:       # Print Position State
                if not pFlag.read():
                    print(position.read())
                    state = S1_CMD
                    
            elif state == S4_DELTA:          # Print Delta State
                if not dFlag.read():
                    print(delta.read())
                    state = S1_CMD
                    
            elif state == S5_GATHER:         # Gather Data State
                if ser.any():
                    charIn = ser.read(1).decode() # need to check for key input (s stops collection)
                    if charIn in {'s', 'S'}: # Stop collecting data and print if s is input
                        gFlag.write(False)
                        for (time,posit) in zip(timeArray[:index],positionArray[:index]):
                            print(f"{(time - start)/1000:.2f}, {posit}")
                        print("End Data")
                        state = S1_CMD                        
                elif timeArray[-1] > 0: # When the last value in the time array
                                        #is rewritten to no longer be 0, print 
                                        #the array. This occurs after 30 seconds have passed.
                    gFlag.write(False)
                    for (time,posit) in zip(timeArray,positionArray):
                        print(f"{(time - start)/1000:.2f}, {posit}")
                    print("End Data")                
                    state = S1_CMD
                                    
                else: # No stop criteria has been met, continue filling arrays and increase the index.
                    timeArray[index] = Time.read()
                    positionArray[index] = Pos.read()
                    index += 1
          # yield state
        else:
            yield None
